﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CredencializacionMaquila
{
    class Plantilla
    {
        CrearPdf pdf = new CrearPdf();

        public void crearPlantilla(List<string> datos)
        {
            pdf.createPdf();
            pdf.openWrite();

            foreach (var label in datos)
            {
                pdf.addLine(label, 1, 1, 90, pdf.fontFormat(true), 13);
            }

            pdf.closeWrite();
            pdf.closePdf();

            pdf.openPdf();
        }
    }
}
