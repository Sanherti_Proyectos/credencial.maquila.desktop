﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Collections.Specialized;
using System.Windows.Forms;
using System.IO;

namespace Sistema_Centralizado
{

    class Configuracion_AppConf
    {
        //frmPrincipal main_form = new frmPrincipal();
        OpenFileDialog ventana_archivo = new OpenFileDialog();
        String nombre_db = "";
        String path = "";
        String cadena = "";
        String seccion_connection = "connectionStrings";
        String nombre_archivo = "";
        String objeto_item = @"C:\Users\r3iva\Pictures\Sistema Centralizado\prueba";
 
        public void master()
        {
            String[] archivos = Directory.GetFiles(@"C:\Users\r3iva\Pictures\Sistema Centralizado\prueba","*.config");
            int cantidad = archivos.Length;
            StringBuilder cadenaConexion = new StringBuilder();
            String query = "INSERT INTO CLIENTE (nombre,stringconnection,parametros) VALUES ('Pablo','blablabla','parametros')";
            //String elementos = "";

            foreach (String elementos in archivos)
            {
                //MessageBox.Show(elementos);
                //cadenaConexion.Append(Obtener_cadenaConfig(elementos));
            }
            //insertar(query);
            
        }

        public string openfile()
        {
            Stream stream = null;

            ventana_archivo.InitialDirectory = "C:\\";
            ventana_archivo.Filter = ".config | *.config";
            String nombre_archivoConfig = "";

            if (ventana_archivo.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((stream = ventana_archivo.OpenFile()) != null)
                    {
                        using (stream)
                        {
                            nombre_archivoConfig = ventana_archivo.FileName;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: No se pudo abrir el archivo " + ex.Message);
                }
            }
            return nombre_archivoConfig;
        }

        public void boton ()
        {
            String file = openfile();
            String cadena = Obtener_cadenaConfig(file);
        }

        public string Obtener_cadenaConfig(String path)
        {
            this.path = path;

            ExeConfigurationFileMap mapa = new ExeConfigurationFileMap { ExeConfigFilename = path };

            Configuration config = ConfigurationManager.OpenMappedExeConfiguration(mapa, ConfigurationUserLevel.None);

            String cadena = config.ConnectionStrings.ConnectionStrings["db"].ConnectionString;
            
            MessageBox.Show(cadena);

            return cadena;
        }

        public void modificar_appConfig(String cadena_conexion)
        {
            
            this.cadena = cadena_conexion;


            Configuration configuracion = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            configuracion.ConnectionStrings.ConnectionStrings["db"].ConnectionString = cadena_conexion;

            configuracion.Save(ConfigurationSaveMode.Modified, true);

            ConfigurationManager.RefreshSection(seccion_connection);

            MessageBox.Show("Exito!");
        }

        public void crear_appConfig(String archivo)
        {
            this.nombre_archivo = archivo;
            String archivo_copy = archivo + "(copy)" + ".config";

            path = @"C:\Users\r3iva\Documents\sistema-centralizado\Sistema Centralizado\appConfig";


            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }

            try
            {
                File.Copy(nombre_archivo, archivo_copy);
            }
            catch (Exception)
            {
                MessageBox.Show("Error al copiar archivo");
            }
        }

        public void agregar_StringConfig()
        {
            

            String parametro = ConfigurationManager.AppSettings["connectionStrings"];
            try
            {
                var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = config.AppSettings.Settings;
                String key = "Setting1";
                String valor = "valor";

                //settings.Add(key, valor);
                settings[key].Value = "valor";

                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(config.AppSettings.SectionInformation.Name);
                MessageBox.Show("Correcto");
            }
            catch(ConfigurationErrorsException)
            {
                MessageBox.Show("Error al escribir");
            }
        }
        
    }
}

