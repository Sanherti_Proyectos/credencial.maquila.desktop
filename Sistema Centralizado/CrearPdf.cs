﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Diagnostics;


namespace CredencializacionMaquila
{
    class CrearPdf
    {
        string filePath;
        public int x { get; set; }
        public int y { get; set; }
        public Document pdf { get; set; }
        public PdfWriter write { get; set; }
        public PdfContentByte cb { get; set; }
        public BaseFont baseFont { get; set; }
        //public FontFactory fontProperties;
        //public FontFactory fontType { get; set; }
        //public Font font { get; set; }
        //public BaseColor fontColor { get; set; }

        /// <summary>
        /// crea y abre un documeto pdf en una ruta temporal
        /// </summary>
        public void createPdf()
        {
            filePath = Path.GetTempPath() + Guid.NewGuid().ToString() + ".pdf";

            //Creamos documento con el tamaña de pagina tradicional
            pdf = new Document(PageSize.A4);//, 60, 60, 100, 10);

            //Indicamos la ruta donde se guardara el documento
            write = PdfWriter.GetInstance(pdf, new FileStream(filePath, FileMode.Create));

            //abrimos archivo
            pdf.Open();
        }

        /// <summary>
        /// Recibe un parametro style que es 0 fuente normal 1 fueten negrita
        /// </summary>
        /// <param name="style"></param>
        /// <returns></returns>
        public BaseFont fontFormat(bool style)
        {
            if (style)
            {
                return baseFont = FontFactory.GetFont(FontFactory.HELVETICA, Font.DEFAULTSIZE, Font.BOLD, BaseColor.WHITE).BaseFont;
            }
            else
            {
                return baseFont = FontFactory.GetFont(FontFactory.HELVETICA, Font.DEFAULTSIZE, Font.NORMAL, BaseColor.WHITE).BaseFont;
            }   
        }

        public void openWrite()
        {
            cb = new PdfContentByte(write);
            cb = write.DirectContent;
            cb.BeginText();
        }

        public void addLine(string text, int x, int y, int rotacion, BaseFont fontFormat, int fontSize)
        {
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, text, x, y, rotacion);
            cb.SetFontAndSize(fontFormat, fontSize);
        }

        public void closeWrite()
        {
            cb.EndText();
        }

        public void closePdf()
        {
            pdf.Close();
        }

        public void openPdf()
        {
            Process prc = new System.Diagnostics.Process();
            prc.StartInfo.FileName = filePath;
            prc.Start();
        }
    }
}
