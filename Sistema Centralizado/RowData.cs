﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;

namespace Sistema_Centralizado
{
    class RowData : Query
    {
        List<string> datosClienteDGV = new List<string>();
        List<string> columnasDGV = new List<string>();
        DataGridView dgv;
        PictureBox pb = new PictureBox();
        DataGridViewCellEventArgs index;
        public string tabla { get; set; }
        public string atributos_insertar { get; set; }
        public string atributos_seleccionar { get; set; }

        #region Set & get
        public List<string> DatosClienteDGV
        {
            get
            {
                return datosClienteDGV;
            }

            set
            {
                datosClienteDGV = value;
            }
        }

        public List<string> ColumnasDGV
        {
            get
            {
                return columnasDGV;
            }

            set
            {
                columnasDGV = value;
            }
        }
        #endregion

        public RowData()
        {
            tabla = "ALUMNO";
            atributos_insertar = cliente_info_atributos_insertar;
            atributos_seleccionar = cliente_info_atributos_seleccionar;
        }

        #region Métodos

        public void modificar()
        {
            inicializarRowData(dgv, index);
            insertar(queryActualizar(tabla, valoresUpdate(), $"{WHERE} {ColumnasDGV[0]} {igual} {c}{DatosClienteDGV[0]}{c}"));
        }

        /// <summary>
        /// Éste método inserta los valores que están en la lista(valores recibidos
        /// por el DGV con el Rowindex seleccionado y todas sus columnas). 
        /// </summary>
        public void cargarLabels(List<Labels> label)
        {
            int incremento = 0;

            foreach (Labels texto_label in label)
            {
                texto_label.Text = DatosClienteDGV[incremento];

                incremento++;
            }
        }

        /// <summary>
        /// Éste método al darle click a una celda agarra todos los datos de las celdas 
        /// </summary>
        /// 
        //public void registrarImpresion()
        //{
        //    int contador = 0;
        //    try
        //    {
        //        contador = Convert.ToInt32(datosClienteDGV[datosClienteDGV.Count - 1]);
        //        contador++;
        //        datosClienteDGV[datosClienteDGV.Count - 1] = contador.ToString();
        //        string fecha = DateTime.Now.ToString();
        //        datosClienteDGV[datosClienteDGV.Count - 2] = fecha;
        //        modificar();
        //    }
        //    catch(FormatException )
        //    {
        //        MessageBox.Show("error al insertar, el valor no es digito");
        //    }
            
        //}

        public string inicializarRowData(DataGridView DGV, DataGridViewCellEventArgs e)
        {
            string datocliente = "";
            this.dgv = DGV;
            this.index = e;
            
            DatosClienteDGV.Clear();
            ColumnasDGV.Clear();
            
            for (int i = 0; i < dgv.Rows[e.RowIndex].Cells.Count; i++)
            {
                datocliente = dgv.Rows[e.RowIndex].Cells[i].Value.ToString();
                

                if (datocliente != "")
                {
                    this.DatosClienteDGV.Add(datocliente);
                }
                else
                {
                    this.DatosClienteDGV.Add("");
                }
                ColumnasDGV.Add(DGV.Rows[e.RowIndex].Cells[i].OwningColumn.Name.ToString());
            }
            
            return DatosClienteDGV[0];
        }

        public void inicializarNombreColumna(DataGridView dgv)
        {
            ColumnasDGV.Clear();

            for (int i = 0; i < dgv.Rows[0].Cells.Count; i++)
            {

                ColumnasDGV.Add(dgv.Rows[0].Cells[i].OwningColumn.Name.ToString());
            }
        }
        #endregion

        #region valores & valoresupdate
        public string valores()
        {
            string cadenaValores = $"{ abrir_parentesis}{c}";
            int contador = 1;

            foreach (var valor in DatosClienteDGV)
            {
                cadenaValores += valor;
                cadenaValores += ccc;

                contador++;

                if(contador == DatosClienteDGV.Count)
                {
                    cadenaValores += $"{c}{cerrar_parentesis}";
                }
            }
            return cadenaValores;
        }

        public string valoresUpdate()
        {
            string cadenaValores = "";
            int contador = 0;

            foreach (var valor in DatosClienteDGV)
            {
                cadenaValores += $"{ColumnasDGV[contador]} {igual} {c}{valor}{c}";
                contador++;

                if (contador < ColumnasDGV.Count)
                {
                    cadenaValores += $"{cc}";
                }
            }

            return cadenaValores;
        }
        #endregion
    }

}
