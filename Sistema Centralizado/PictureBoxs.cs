﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace Sistema_Centralizado
{
    public class PictureBoxs : PictureBox
    {
        public PictureBox nuevo_pb, nuevo_pbActual;
        Labels label = new Labels();
        List<PictureBox> listaPictureBox = new List<PictureBox>();
        public PictureBox pbSelect { get; set; }
        int ubicacion_pb_X = 50;
        int ubicacion_pb_Y = 50;
        Point mousePosicion;
        Cliente_info cliente_info = new Cliente_info();

        public void NuevoPictureBox_Click()
        {

            nuevo_pb = new PictureBox();
            nuevo_pb.Location = new Point(ubicacion_pb_X, ubicacion_pb_Y);
            nuevo_pb.Size = new Size(86, 103);
            nuevo_pb.SizeMode = PictureBoxSizeMode.StretchImage;
            nuevo_pb.BackColor = Color.Aqua;
            nuevo_pb.BringToFront();

            nuevo_pb.MouseDown += new MouseEventHandler(nuevo_pb_MouseDown);
            nuevo_pb.MouseMove += new MouseEventHandler(nuevo_pb_MouseMove);
            nuevo_pb.Click += new EventHandler(nuevo_pb_Click);
            nuevo_pb.MouseDoubleClick += new MouseEventHandler(nuevo_pb_DoubleClick);
            nuevo_pb.KeyPress += new KeyPressEventHandler(nuevo_pb_KeyPress);

            try
            {
                pbSelect.Controls.Add(nuevo_pb);
                listaPictureBox.Add(nuevo_pb);
                ubicacion_pb_Y += 50;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Selecciona un PictureBox");
            }
        }

        public void reacomodo_label(List<string> parametros, PictureBox pb)
        {
            nuevo_pb = new PictureBox();
            nuevo_pb.Click += new EventHandler(nuevo_pb_Click);
            nuevo_pb.MouseDown += new MouseEventHandler(nuevo_pb_MouseDown);
            nuevo_pb.MouseMove += new MouseEventHandler(nuevo_pb_MouseMove);
            nuevo_pb.MouseDoubleClick += new MouseEventHandler(nuevo_pb_DoubleClick);

            int parametroX = 0;
            int parametroY = 0;
            int num_etiq = 0;
            int bandera = 0;

            foreach (var list in parametros)
            {
                if (bandera == 0)
                {
                    parametroX = Int32.Parse(list);
                }

                if (bandera == 1)
                {
                    parametroY = Int32.Parse(list);
                }

                bandera++;

                if (bandera == 2)
                {
                    try
                    {
                        nuevo_pb.Location = new Point(parametroX, parametroY);
                        nuevo_pb.Text = "Label" + num_etiq;
                        nuevo_pb.BackColor = Color.Transparent;
                        nuevo_pb.AutoSize = true;
                        pb.Controls.Add(nuevo_pb);
                        listaPictureBox.Add(nuevo_pb);
                        nuevo_pb = new PictureBoxs();
                        num_etiq++;
                        bandera = 0;

                        nuevo_pb.Click += new EventHandler(nuevo_pb_Click);
                        nuevo_pb.MouseDown += new MouseEventHandler(nuevo_pb_MouseDown);
                        nuevo_pb.MouseMove += new MouseEventHandler(nuevo_pb_MouseMove);
                        nuevo_pb.MouseDoubleClick += new MouseEventHandler(nuevo_pb_DoubleClick);
                         
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show("Error selecciona un PictureBox para mostrar los Labels");
                    }

                }
            }

        }

        public List<string> masterPictureBox(int valor_referencia, string nombre_cliente)
        {
            string posicion_picturebox = cliente_info.obtener_StringParametros(nombre_cliente);

            List<string> pbIzq = new List<string>();
            List<string> pbDer = new List<string>();
            Char pipe = '|';
            Char guion = '-';
            Char coma = ',';
            string[] picturebox = { };
            string[] final = { };
            string pb_izq = "";
            string pb_der = "";
            int cont = 0;

            picturebox = posicion_picturebox.Split(pipe);
            pb_izq = picturebox[0];
            pb_der = picturebox[1];

            Array.Clear(picturebox, 0, picturebox.Length);

            picturebox = pb_izq.Split(guion);

            foreach (var mov in picturebox)
            {
                final = mov.Split(coma);

                foreach (var mov_1 in final)
                {
                    pbIzq.Add(mov_1);
                }

                Array.Clear(final, 0, final.Length);
            }

            Array.Clear(final, 0, final.Length);
            Array.Clear(picturebox, 0, final.Length);

            picturebox = pb_der.Split(guion);

            foreach (var mov in picturebox)
            {
                final = mov.Split(coma);

                foreach (var mov_1 in final)
                {
                    pbDer.Add(mov_1);
                }

                Array.Clear(final, 0, final.Length);
            }

            if (valor_referencia == 0)
                return pbIzq;

            else if (valor_referencia == 1)
                return pbDer;

            else
            {
                MessageBox.Show("Referencia incorrecta");
                return null;
            }


        }

        private void pictureBox_Click(object sender, EventArgs e)
        {
            pbSelect = sender as PictureBox;

            string nombrepicturebox = pbSelect.Name;
        }

        private void nuevo_pb_Click(object sender, EventArgs e)
        {
            nuevo_pbActual = sender as PictureBox;
        }

        private void nuevo_pb_MouseDown(object sender, MouseEventArgs e)
        {
            mousePosicion = e.Location;
        }

        private void nuevo_pb_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                int dx = e.X - mousePosicion.X;
                int dy = e.Y - mousePosicion.Y;
                nuevo_pbActual = sender as PictureBox;

                nuevo_pbActual.Location = new Point(nuevo_pbActual.Left + dx, nuevo_pbActual.Top + dy);
                nuevo_pbActual.BringToFront();
            }

        }

        public void nuevo_pb_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                nuevo_pbActual.Image = Image.FromFile(abrirFoto());
            }
            catch (Exception exc)
            {
                MessageBox.Show("No se pudo cargar la imagen");
            }

        }

        public void nuevo_pb_eliminar()
        {
            try
            {
                pbSelect.Controls.Remove(nuevo_pbActual);
                listaPictureBox.Remove(nuevo_pbActual);
            }
            catch (Exception ex)
            {

            }
        }

        public void nuevo_pb_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.C)
            {
                pbSelect.Controls.Remove(nuevo_pbActual);
            }
        }

        private void pbFrente_Click(object sender, EventArgs e)
        {
            label.etiquetaActual = null;
            nuevo_pbActual = null;
        }

        private void pbVuelta_Click(object sender, EventArgs e)
        {
            label.etiquetaActual = null;
            nuevo_pbActual = null;
        }

        public String abrirFoto()
        {

            Stream stream = null;
            OpenFileDialog ventana_imagen = new OpenFileDialog();
            string ruta_archivo = "";

            ventana_imagen.InitialDirectory = "C:\\Imagenes\\";
            ventana_imagen.Filter = "Tipo de imagenes .jpeg .png .jpg | *.jpeg;*.png;*.jpg";

            if (ventana_imagen.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((stream = ventana_imagen.OpenFile()) != null)
                    {
                        using (stream)
                        {
                            ruta_archivo = ventana_imagen.FileName;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: No se pudo abrir el archivo " + ex.Message);
                }
            }

            return ruta_archivo;
        }


    }
}
