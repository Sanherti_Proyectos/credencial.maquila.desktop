﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sistema_Centralizado
{
    public class Labels : Label
    {
        public int rotateAngle { get; set; }  // rotar texto
        public string newText { get; set; }   // escribir texto
        string temporal = "";

        private string font_size = "";
        private string rotation = "";
        private string position = "";

        int id = 0, incrementoPosicion = 0;
        public bool clicLabel { get; set; } = false;

        public List<Labels> listaLabel { get; set; } = new List<Labels>();

        #region Set & Get
        public string Font_size
        {
            get
            {
                return font_size;
            }

            set
            {
                font_size = value;
            }
        }

        public string Rotation
        {
            get
            {
                return rotation;
            }

            set
            {
                rotation = value;
            }
        }

        public string Position
        {
            get
            {
                return position;
            }

            set
            {
                position = value;
            }
        }

        public string Listanombrelb
        {
            get
            {
                return listanombrelb;
            }

            set
            {
                listanombrelb = value;
            }
        }
        #endregion

        private string listanombrelb = "";

        PictureBox pb = new PictureBox();
        TextBox txtetiqueta = new TextBox();
        TextBox txinfinito = new TextBox();
        Point mousePosicion;//mousePosicionPictureBox;
        public Labels etiqueta, etiquetaActual;



        /// <summary>
        /// Crea etiquetas recibe como parametros el objeto donde seran colocados 
        /// y el nombre de la etiqueta
        /// </summary>
        /// <param name="pb"></param>
        /// <param name="nombreEtiqueta"></param>
        /// 
        //public void generarLabel(PictureBox pb, string nombreEtiqueta, int rotacion)
        public void generarLabel(PictureBox pb, string nombreEtiqueta, string x, string y, int rotacion)
        {
            this.pb = pb;

            etiqueta = new Labels();
            etiqueta.Location = new Point(115, 0 + (25 * incrementoPosicion));
            etiqueta.Name = nombreEtiqueta;
            
            etiqueta.Text = nombreEtiqueta;
            etiqueta.AutoSize = true;
            etiqueta.Width = 200;
            etiqueta.Height = 100;
            etiqueta.rotateAngle = 0;
            etiqueta.BackColor = Color.Transparent;


            etiqueta.Click += new EventHandler(etiqueta_Click);
            etiqueta.MouseDown += new MouseEventHandler(etiqueta_MouseDown);
            etiqueta.MouseMove += new MouseEventHandler(etiqueta_MouseMove);
            etiqueta.MouseDoubleClick += new MouseEventHandler(etiqueta_MouseDoubleClick);


            try
            {
                pb.Controls.Add(etiqueta);
                listaLabel.Add(etiqueta);
                listanombrelb += etiqueta.Name;
                listanombrelb += ", ";
                incrementoPosicion++;
            }
            catch (Exception e)
            {
                MessageBox.Show("Selecciona un area para insertar el Label");
            }
            
            if (incrementoPosicion > 7)
            {
                incrementoPosicion = 0;
            }
        }

        public void generarLabel(PictureBox pb, string nombreEtiqueta, string x, string y, int rotacion, string[] configuracionLabel)
        {
            this.pb = pb;

            etiqueta = new Labels();
            etiqueta.Location = new Point(115, 0 + (25 * incrementoPosicion));
            etiqueta.Name = nombreEtiqueta;

            etiqueta.Text = nombreEtiqueta;
            etiqueta.AutoSize = true;
            etiqueta.Width = 200;
            etiqueta.Height = 100;
            etiqueta.rotateAngle = 0;
            etiqueta.BackColor = Color.Transparent;


            etiqueta.Click += new EventHandler(etiqueta_Click);
            etiqueta.MouseDown += new MouseEventHandler(etiqueta_MouseDown);
            etiqueta.MouseMove += new MouseEventHandler(etiqueta_MouseMove);
            etiqueta.MouseDoubleClick += new MouseEventHandler(etiqueta_MouseDoubleClick);


            try
            {
                pb.Controls.Add(etiqueta);
                listaLabel.Add(etiqueta);
                listanombrelb += etiqueta.Name;
                listanombrelb += ", ";
                incrementoPosicion++;
            }
            catch (Exception e)
            {
                MessageBox.Show("Selecciona un area para insertar el Label");
            }

            if (incrementoPosicion > 7)
            {
                incrementoPosicion = 0;
            }
        }

        public List<string> posiciones_Labels(string posiciones,int x_y)
        {
            List<string> posicion_x = new List<string>();
            List<string> posicion_y= new List<string>();
            string[] guiones = { };
            string[] comas = { };
            string[] waiting = { };
            char guion = '-';
            char coma = ',';

            guiones = posiciones.Split(guion);

            foreach(var mov in guiones)
            {
                waiting = mov.Split(coma);

                posicion_x.Add(waiting[0]);
                posicion_y.Add(waiting[1]);

                Array.Clear(waiting, 0, waiting.Length);
            }

            if(x_y==0)
            {
                return posicion_x;
            }

            else
            {
                return posicion_y;
            }

            return null;  
        }

        /// <summary>
        ///Metodo para crear labels Recibe como parametro la lista de coordenada
        /// </summary>
        /// <param name="parametros"></param>
        /// <param name="pb"></param>
        /// 
        public void reacomodo_label(List<string> parametros, PictureBox pb, List<string> x, List<string> y)
        {
            int incremento = 0;

            if (x.Count !=0 && y.Count !=0)
            {
                
                foreach (string dato in parametros)
                {
                    generarLabel(pb, parametros[incremento], x[incremento], y[incremento], 0);

                    incremento++;
                }
            }

            else
            {
                foreach (string dato in parametros)
                {
                    generarLabel(pb, parametros[incremento], "115", Convert.ToString(0 + (25 * incrementoPosicion)), 0);

                    incremento++;
                }
                
            }
        }

        public List<string> posiciones(string x_y, string lado, string posicion)
        {
            //string posicion_labels = cliente_info.obtener_StringParametros(posicion);

            //List<string> lbIzq = new List<string>();
            //List<string> lbDer = new List<string>();
            Char pipe = '|';
            Char guion = '-';
            Char coma = ',';
            string[] izquierdo_derecho = { };
            string[] label = { };
            List<string> posicion_x_y = new List<string>();
            string[] valores = { };
            int incremento = 0;

            //string lab_der = "";
            //int cont = 0;

            //entra x1,y1-x1,y1|x2,y2-x2,y2            
            izquierdo_derecho = posicion.Split(pipe);
            //devuelve [0]x1,y1-x1,y1
            //devuelve [1]x2,y2-x2,y2

            if (lado == "izquierdo")
            {
                //entra x1,y1-x1,y1
                label = izquierdo_derecho[0].Split(guion);
                //devuelve [0]x1,y1
                //devuelve [1]x1,y1
            }
            else if (lado == "derecho")
            {
                //devuelve x2,y2-x2,y2
                label = izquierdo_derecho[1].Split(guion);
                //devuelve [0]x2,y2
                //devuelve [1]x2,y2
            }

            if (x_y == "x")
            {
                for (int i = 0; i < label.Count(); i++)
                {
                    valores = label[i].Split(coma);

                    posicion_x_y.Add(valores[0]);

                    valores = null;
                }
            }
            else if (x_y == "y")
            {
                for (int i = 0; i < label.Count(); i++)
                {
                    valores = label[i].Split(coma);

                    posicion_x_y.Add(valores[1]);

                    valores = null;
                }
            }

            return posicion_x_y;
        }
        

            /// <summary>
            /// devuelve el indice de la etiqueta actual
            /// </summary>
            /// <returns></returns>
        public int indexEtiquetaActual()
        {
            return listaLabel.IndexOf(etiquetaActual);
        }

        /// <summary>
        /// Metodo OnPaint para escribir la propiedad de rotacion
        /// en las nuevas etiquetas
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPaint(PaintEventArgs e)
        {
            Brush b = new SolidBrush(this.ForeColor);
            e.Graphics.TranslateTransform(this.Width / 2, this.Height / 2);
            e.Graphics.RotateTransform(this.rotateAngle);
            e.Graphics.DrawString(this.newText, this.Font, b, 0f, 0f);
            base.OnPaint(e);
        }

        public void etiquetaDelete(Control pb)
        {
            pb.Controls.Remove(listaLabel[indexEtiquetaActual()]);
            listaLabel.Remove(listaLabel[indexEtiquetaActual()]);
        }

        #region EVENTOS
        private void etiquta_KeyPres(object sender, PreviewKeyDownEventArgs e)
        {
            etiquetaActual.Text += e.KeyValue.ToString();
        }

        private void etiqueta_Click(object sender, EventArgs e)
        {
            etiquetaActual = sender as Labels;
            clicLabel = true;   
        }
        

        private void etiqueta_MouseDoubleClick(object sender, EventArgs e)
        {
            txtetiqueta = new TextBox();

            etiquetaActual.Visible = false;

            
            txtetiqueta.Visible = true;
            txtetiqueta.BringToFront();
            txtetiqueta.Focus();
            txtetiqueta.KeyPress += new KeyPressEventHandler(txtetiqueta_KeyPress);
            txtetiqueta.Location = listaLabel[indexEtiquetaActual()].Location;//etiquetaActual.Location;
            pb.Controls.Add(txtetiqueta);
        }

        private void txtetiqueta_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                txtetiqueta.Visible = false;
                temporal = txtetiqueta.Text;

                etiquetaActual.Visible = true;

                if (txtetiqueta.Text != "")
                {
                    etiquetaActual.Text = temporal;
                    temporal = "";
                    txtetiqueta.Text = "";
                }
            }
        }
        

        private void etiqueta_MouseDown(object sender, MouseEventArgs e)
        {
            mousePosicion = e.Location;
        }
        
        private void etiqueta_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                int dx = e.X - mousePosicion.X;
                int dy = e.Y - mousePosicion.Y;
                etiquetaActual = sender as Labels;
                int index = indexEtiquetaActual();
                
                listaLabel[index].Location = new Point(listaLabel[index].Left + dx, listaLabel[index].Top + dy);
                etiquetaActual.BringToFront();
            }
        }

        #endregion

        #region METODO PROPIEDADES POSICION, ROTACION, TAMAÑO DE FUENTE

        /// <summary>
        /// Devuelve una cadena de texto con la posicion de las etiquetas
        /// almacenadas en una conleccion
        /// </summary>
        /// <returns></returns>
        public string stringPotitionLabel()
        {
            int incremento = 1;

            foreach (var label in listaLabel)
            {
                position += $"{label.Location.X},{label.Location.Y}";

                if (incremento != listaLabel.Count)
                {
                    position += "-";
                }

                incremento++;
            }

            return position;
        }

        /// <summary>
        /// Devuelve una cadena de texto con la propiedad de rotacion de las etiquetas
        /// almacenadas en una conleccion
        /// </summary>
        /// <returns></returns>
        public string stringRotationLabel()
        {
            int incremento = 1;

            foreach (var label in listaLabel)
            {
                rotation += $"{label.rotateAngle}";

                if (incremento != listaLabel.Count)
                {
                    rotation += ",";
                }

                incremento++;
            }

            return rotation;
        }

        /// <summary>
        /// Devuelve una cadena de texto con el tamaño de fuente de las etiquetas
        /// almacenadas en una conleccion
        /// </summary>
        /// <returns></returns>
        public string stringFontSizeLabel()
        {
            int incremento = 1;

            foreach (var label in listaLabel)
            {
                font_size += $"{label.Font.Size}";

                if (incremento != listaLabel.Count)
                {
                    font_size += ",";
                }

                incremento++;
            }

            return font_size;
        }
        #endregion

    }
}
