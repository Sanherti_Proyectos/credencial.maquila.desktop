﻿using System.Windows.Forms;

namespace Sistema_Centralizado
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrincipal));
            this.cbBasedeDatos = new System.Windows.Forms.ComboBox();
            this.tsPrincipal = new System.Windows.Forms.ToolStrip();
            this.tsbImprimir = new System.Windows.Forms.ToolStripButton();
            this.tsbInsertarRegistro = new System.Windows.Forms.ToolStripButton();
            this.tsbVerDiseño = new System.Windows.Forms.ToolStripButton();
            this.tsbLimpiar = new System.Windows.Forms.ToolStripButton();
            this.tsbCambio = new System.Windows.Forms.ToolStripButton();
            this.tsbInsertarLabel = new System.Windows.Forms.ToolStripButton();
            this.tsbSubirFoto = new System.Windows.Forms.ToolStripButton();
            this.Nuevo_PictureBox = new System.Windows.Forms.ToolStripButton();
            this.tsbEliminar_elemento = new System.Windows.Forms.ToolStripButton();
            this.tsbRotacionLabel = new System.Windows.Forms.ToolStripButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgvBDcliente = new System.Windows.Forms.DataGridView();
            this.gbPlantilla = new System.Windows.Forms.GroupBox();
            this.comboBox_der = new System.Windows.Forms.ComboBox();
            this.comboBox_izq = new System.Windows.Forms.ComboBox();
            this.Cambio = new System.Windows.Forms.Button();
            this.pbVuelta = new System.Windows.Forms.PictureBox();
            this.pbFrente = new System.Windows.Forms.PictureBox();
            this.btInsertarConfigCliente = new System.Windows.Forms.Button();
            this.tsPrincipal.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBDcliente)).BeginInit();
            this.gbPlantilla.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbVuelta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbFrente)).BeginInit();
            this.SuspendLayout();
            // 
            // cbBasedeDatos
            // 
            this.cbBasedeDatos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbBasedeDatos.FormattingEnabled = true;
            this.cbBasedeDatos.Location = new System.Drawing.Point(6, 38);
            this.cbBasedeDatos.Name = "cbBasedeDatos";
            this.cbBasedeDatos.Size = new System.Drawing.Size(489, 23);
            this.cbBasedeDatos.TabIndex = 11;
            this.cbBasedeDatos.SelectedIndexChanged += new System.EventHandler(this.cbBasedeDatos_SelectedIndexChanged);
            // 
            // tsPrincipal
            // 
            this.tsPrincipal.BackColor = System.Drawing.SystemColors.HotTrack;
            this.tsPrincipal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbImprimir,
            this.tsbInsertarRegistro,
            this.tsbVerDiseño,
            this.tsbLimpiar,
            this.tsbCambio,
            this.tsbInsertarLabel,
            this.tsbSubirFoto,
            this.Nuevo_PictureBox,
            this.tsbEliminar_elemento,
            this.tsbRotacionLabel});
            this.tsPrincipal.Location = new System.Drawing.Point(0, 0);
            this.tsPrincipal.Name = "tsPrincipal";
            this.tsPrincipal.Size = new System.Drawing.Size(1066, 72);
            this.tsPrincipal.TabIndex = 69;
            this.tsPrincipal.Text = "Eliminar Etiqueta";
            // 
            // tsbImprimir
            // 
            this.tsbImprimir.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsbImprimir.Image = ((System.Drawing.Image)(resources.GetObject("tsbImprimir.Image")));
            this.tsbImprimir.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbImprimir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbImprimir.Name = "tsbImprimir";
            this.tsbImprimir.Size = new System.Drawing.Size(60, 69);
            this.tsbImprimir.Text = "Imprimir";
            this.tsbImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbImprimir.Click += new System.EventHandler(this.tsbImprimir_Click);
            // 
            // tsbInsertarRegistro
            // 
            this.tsbInsertarRegistro.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsbInsertarRegistro.Image = ((System.Drawing.Image)(resources.GetObject("tsbInsertarRegistro.Image")));
            this.tsbInsertarRegistro.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbInsertarRegistro.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbInsertarRegistro.Name = "tsbInsertarRegistro";
            this.tsbInsertarRegistro.Size = new System.Drawing.Size(105, 69);
            this.tsbInsertarRegistro.Text = "Insertar Registro";
            this.tsbInsertarRegistro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbInsertarRegistro.Click += new System.EventHandler(this.tsbInsertarRegistro_Click);
            // 
            // tsbVerDiseño
            // 
            this.tsbVerDiseño.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsbVerDiseño.Image = ((System.Drawing.Image)(resources.GetObject("tsbVerDiseño.Image")));
            this.tsbVerDiseño.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbVerDiseño.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbVerDiseño.Name = "tsbVerDiseño";
            this.tsbVerDiseño.Size = new System.Drawing.Size(71, 69);
            this.tsbVerDiseño.Text = "Ver Diseño";
            this.tsbVerDiseño.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbVerDiseño.Click += new System.EventHandler(this.tsbVerDiseño_Click);
            // 
            // tsbLimpiar
            // 
            this.tsbLimpiar.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsbLimpiar.Image = ((System.Drawing.Image)(resources.GetObject("tsbLimpiar.Image")));
            this.tsbLimpiar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbLimpiar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbLimpiar.Name = "tsbLimpiar";
            this.tsbLimpiar.Size = new System.Drawing.Size(87, 69);
            this.tsbLimpiar.Text = "Limpiar Datos";
            this.tsbLimpiar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tsbCambio
            // 
            this.tsbCambio.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsbCambio.Image = ((System.Drawing.Image)(resources.GetObject("tsbCambio.Image")));
            this.tsbCambio.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbCambio.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbCambio.Name = "tsbCambio";
            this.tsbCambio.Size = new System.Drawing.Size(108, 69);
            this.tsbCambio.Text = "Diseño horizontal";
            this.tsbCambio.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbCambio.Click += new System.EventHandler(this.tsbCambio_Click);
            // 
            // tsbInsertarLabel
            // 
            this.tsbInsertarLabel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsbInsertarLabel.Image = ((System.Drawing.Image)(resources.GetObject("tsbInsertarLabel.Image")));
            this.tsbInsertarLabel.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbInsertarLabel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbInsertarLabel.Name = "tsbInsertarLabel";
            this.tsbInsertarLabel.Size = new System.Drawing.Size(77, 69);
            this.tsbInsertarLabel.Text = "Nuevo label";
            this.tsbInsertarLabel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbInsertarLabel.Click += new System.EventHandler(this.tsbInsertarLabel_Click);
            // 
            // tsbSubirFoto
            // 
            this.tsbSubirFoto.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbSubirFoto.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsbSubirFoto.Image = ((System.Drawing.Image)(resources.GetObject("tsbSubirFoto.Image")));
            this.tsbSubirFoto.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbSubirFoto.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSubirFoto.Name = "tsbSubirFoto";
            this.tsbSubirFoto.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.tsbSubirFoto.Size = new System.Drawing.Size(68, 69);
            this.tsbSubirFoto.Text = "Subir Foto";
            this.tsbSubirFoto.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // Nuevo_PictureBox
            // 
            this.Nuevo_PictureBox.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Nuevo_PictureBox.Image = ((System.Drawing.Image)(resources.GetObject("Nuevo_PictureBox.Image")));
            this.Nuevo_PictureBox.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.Nuevo_PictureBox.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Nuevo_PictureBox.Name = "Nuevo_PictureBox";
            this.Nuevo_PictureBox.Size = new System.Drawing.Size(113, 69);
            this.Nuevo_PictureBox.Text = "Nuevo PictureBox";
            this.Nuevo_PictureBox.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Nuevo_PictureBox.ToolTipText = "Nuevo PictureBox";
            this.Nuevo_PictureBox.Click += new System.EventHandler(this.Agregar_PictureBox);
            // 
            // tsbEliminar_elemento
            // 
            this.tsbEliminar_elemento.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsbEliminar_elemento.Image = ((System.Drawing.Image)(resources.GetObject("tsbEliminar_elemento.Image")));
            this.tsbEliminar_elemento.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbEliminar_elemento.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbEliminar_elemento.Name = "tsbEliminar_elemento";
            this.tsbEliminar_elemento.Size = new System.Drawing.Size(104, 69);
            this.tsbEliminar_elemento.Text = "Eliminar Etiqueta";
            this.tsbEliminar_elemento.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbEliminar_elemento.ToolTipText = "Eliminar Elemento";
            this.tsbEliminar_elemento.Click += new System.EventHandler(this.tsbEliminarEtiqueta_Click);
            // 
            // tsbRotacionLabel
            // 
            this.tsbRotacionLabel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsbRotacionLabel.Image = ((System.Drawing.Image)(resources.GetObject("tsbRotacionLabel.Image")));
            this.tsbRotacionLabel.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbRotacionLabel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbRotacionLabel.Name = "tsbRotacionLabel";
            this.tsbRotacionLabel.Size = new System.Drawing.Size(92, 69);
            this.tsbRotacionLabel.Text = "Rotar etiqueta";
            this.tsbRotacionLabel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgvBDcliente);
            this.groupBox1.Controls.Add(this.cbBasedeDatos);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(5, 90);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(502, 413);
            this.groupBox1.TabIndex = 70;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Base de datos";
            // 
            // dgvBDcliente
            // 
            this.dgvBDcliente.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.dgvBDcliente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBDcliente.Location = new System.Drawing.Point(6, 67);
            this.dgvBDcliente.Name = "dgvBDcliente";
            this.dgvBDcliente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvBDcliente.Size = new System.Drawing.Size(489, 340);
            this.dgvBDcliente.TabIndex = 73;
            this.dgvBDcliente.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBDcliente_CellContentClick_1);
            this.dgvBDcliente.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBDcliente_CellEndEdit);
            // 
            // gbPlantilla
            // 
            this.gbPlantilla.Controls.Add(this.comboBox_der);
            this.gbPlantilla.Controls.Add(this.comboBox_izq);
            this.gbPlantilla.Controls.Add(this.Cambio);
            this.gbPlantilla.Controls.Add(this.pbVuelta);
            this.gbPlantilla.Controls.Add(this.pbFrente);
            this.gbPlantilla.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbPlantilla.Location = new System.Drawing.Point(541, 75);
            this.gbPlantilla.Name = "gbPlantilla";
            this.gbPlantilla.Size = new System.Drawing.Size(478, 428);
            this.gbPlantilla.TabIndex = 71;
            this.gbPlantilla.TabStop = false;
            this.gbPlantilla.Text = "Diseño Vertical";
            // 
            // comboBox_der
            // 
            this.comboBox_der.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_der.FormattingEnabled = true;
            this.comboBox_der.Location = new System.Drawing.Point(278, 25);
            this.comboBox_der.Name = "comboBox_der";
            this.comboBox_der.Size = new System.Drawing.Size(154, 23);
            this.comboBox_der.TabIndex = 75;
            this.comboBox_der.SelectedIndexChanged += new System.EventHandler(this.comboBox_der_SelectedIndexChanged);
            // 
            // comboBox_izq
            // 
            this.comboBox_izq.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_izq.FormattingEnabled = true;
            this.comboBox_izq.Location = new System.Drawing.Point(15, 25);
            this.comboBox_izq.Name = "comboBox_izq";
            this.comboBox_izq.Size = new System.Drawing.Size(154, 23);
            this.comboBox_izq.TabIndex = 74;
            this.comboBox_izq.SelectedIndexChanged += new System.EventHandler(this.comboBox_izq_SelectedIndexChanged);
            // 
            // Cambio
            // 
            this.Cambio.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Cambio.Location = new System.Drawing.Point(180, 0);
            this.Cambio.Name = "Cambio";
            this.Cambio.Size = new System.Drawing.Size(92, 32);
            this.Cambio.TabIndex = 2;
            this.Cambio.Text = "Cambio";
            this.Cambio.UseVisualStyleBackColor = true;
            this.Cambio.Click += new System.EventHandler(this.Cambio_Click);
            // 
            // pbVuelta
            // 
            this.pbVuelta.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pbVuelta.Location = new System.Drawing.Point(242, 61);
            this.pbVuelta.Name = "pbVuelta";
            this.pbVuelta.Size = new System.Drawing.Size(230, 361);
            this.pbVuelta.TabIndex = 1;
            this.pbVuelta.TabStop = false;
            // 
            // pbFrente
            // 
            this.pbFrente.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pbFrente.Location = new System.Drawing.Point(6, 61);
            this.pbFrente.Name = "pbFrente";
            this.pbFrente.Size = new System.Drawing.Size(230, 361);
            this.pbFrente.TabIndex = 0;
            this.pbFrente.TabStop = false;
            // 
            // btInsertarConfigCliente
            // 
            this.btInsertarConfigCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btInsertarConfigCliente.Location = new System.Drawing.Point(979, 75);
            this.btInsertarConfigCliente.Name = "btInsertarConfigCliente";
            this.btInsertarConfigCliente.Size = new System.Drawing.Size(87, 55);
            this.btInsertarConfigCliente.TabIndex = 72;
            this.btInsertarConfigCliente.Text = "save config.";
            this.btInsertarConfigCliente.UseVisualStyleBackColor = true;
            this.btInsertarConfigCliente.Click += new System.EventHandler(this.btInsertarConfigCliente_Click);
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1066, 515);
            this.Controls.Add(this.btInsertarConfigCliente);
            this.Controls.Add(this.gbPlantilla);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tsPrincipal);
            this.Name = "frmPrincipal";
            this.Text = "frmPrincipal";
            this.tsPrincipal.ResumeLayout(false);
            this.tsPrincipal.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBDcliente)).EndInit();
            this.gbPlantilla.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbVuelta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbFrente)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbBasedeDatos;
        private System.Windows.Forms.ToolStrip tsPrincipal;
        private System.Windows.Forms.ToolStripButton tsbImprimir;
        private System.Windows.Forms.ToolStripButton tsbInsertarRegistro;
        private System.Windows.Forms.ToolStripButton tsbVerDiseño;
        private System.Windows.Forms.ToolStripButton tsbCambio;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox gbPlantilla;
        private System.Windows.Forms.PictureBox pbVuelta;
        private System.Windows.Forms.PictureBox pbFrente;
        private System.Windows.Forms.DataGridView dgvBDcliente;
        private System.Windows.Forms.ToolStripButton tsbLimpiar;
        private System.Windows.Forms.ToolStripButton tsbInsertarLabel;
        private System.Windows.Forms.ToolStripButton tsbSubirFoto;
        private System.Windows.Forms.ToolStripButton tsbRotacionLabel;
        private ToolStripButton tsbEliminar_elemento;
        private Button Cambio;
        private ToolStripButton Nuevo_PictureBox;
        private ComboBox comboBox_izq;
        private ComboBox comboBox_der;
        private Button btInsertarConfigCliente;

        public ComboBox CbBasedeDatos
        {
            get
            {
                return cbBasedeDatos;
            }

            set
            {
                cbBasedeDatos = value;
            }
        }

        public ComboBox ComboBox_izq
        {
            get
            {
                return comboBox_izq;
            }

            set
            {
                comboBox_izq = value;
            }
        }

        public ComboBox ComboBox_der
        {
            get
            {
                return comboBox_der;
            }

            set
            {
                comboBox_der = value;
            }
        }

        public ComboBox ComboBox_der1
        {
            get
            {
                return comboBox_der;
            }

            set
            {
                comboBox_der = value;
            }
        }
    }
}