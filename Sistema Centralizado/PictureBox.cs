using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Sistema_Centralizado
{
    public class PB
    {
        System.Windows.Forms.PictureBox nuevo_pb, nuevo_pbActual;
        //Labels label = new Labels();
        List<PictureBox> listaPictureBox = new List<PictureBox>();
        public PictureBox pbActual { get; set; }
        int ubicacion_pb_X = 50;
        int ubicacion_pb_Y = 50;
        Point mousePosicion;

        public void NuevoPictureBox_Click()
        {
            nuevo_pb = new PictureBox();
            nuevo_pb.Location = new Point(ubicacion_pb_X, ubicacion_pb_Y);
            nuevo_pb.Size = new Size(50, 50);
            nuevo_pb.SizeMode = PictureBoxSizeMode.StretchImage;
            nuevo_pb.BackColor = Color.Aqua;
            nuevo_pb.BringToFront();

            nuevo_pb.MouseDown += new MouseEventHandler(nuevo_pb_MouseDown);
            nuevo_pb.MouseMove += new MouseEventHandler(nuevo_pb_MouseMove);
            nuevo_pb.Click += new EventHandler(nuevo_pb_Click);
            nuevo_pb.MouseDoubleClick += new MouseEventHandler(nuevo_pb_DoubleClick);
            nuevo_pb.KeyPress += new KeyPressEventHandler(nuevo_pb_KeyPress);

            try
            {
                pbActual.Controls.Add(nuevo_pb);
                listaPictureBox.Add(nuevo_pb);
                ubicacion_pb_Y += 50;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Selecciona un PictureBox");
            }
        }

        private void pictureBox_Click(object sender, EventArgs e)
        {
            pbActual = sender as PictureBox;

            string nombrepicturebox = pbActual.Name;
        }

        private void nuevo_pb_Click(object sender, EventArgs e)
        {
            nuevo_pbActual = sender as PictureBox;
        }

        private void nuevo_pb_MouseDown(object sender, MouseEventArgs e)
        {
            mousePosicion = e.Location;
        }

        private void nuevo_pb_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                int dx = e.X - mousePosicion.X;
                int dy = e.Y - mousePosicion.Y;
                nuevo_pbActual = sender as System.Windows.Forms.PictureBox;

                nuevo_pbActual.Location = new Point(nuevo_pbActual.Left + dx, nuevo_pbActual.Top + dy);
                nuevo_pbActual.BringToFront();
            }
        }

        public void nuevo_pb_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                nuevo_pbActual.Image = Image.FromFile(abrirFoto());
            }
            catch (Exception exc)
            {
                MessageBox.Show("No se pudo cargar la imagen");
            }
        }

        public void nuevo_pb_eliminar()
        {
            try
            {
                pbActual.Controls.Remove(nuevo_pbActual);
                listaPictureBox.Remove(nuevo_pbActual);
            }
            catch (Exception ex)
            {

            }
        }

        public void nuevo_pb_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.C)
            {
                pbActual.Controls.Remove(nuevo_pbActual);
            }
        }

        private void pbFrente_Click(object sender, EventArgs e)
        {
            //label.etiquetaActual = null;
            nuevo_pbActual = null;
        }

        private void pbVuelta_Click(object sender, EventArgs e)
        {
            //label.etiquetaActual = null;
            nuevo_pbActual = null;
        }

        public String abrirFoto()
        {

            Stream stream = null;
            OpenFileDialog ventana_imagen = new OpenFileDialog();
            string ruta_archivo = "";

            ventana_imagen.InitialDirectory = "C:\\Imagenes\\";
            ventana_imagen.Filter = "Tipo de imagenes .jpeg .png .jpg | *.jpeg;*.png;*.jpg";

            if (ventana_imagen.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((stream = ventana_imagen.OpenFile()) != null)
                    {
                        using (stream)
                        {
                            ruta_archivo = ventana_imagen.FileName;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: No se pudo abrir el archivo " + ex.Message);
                }
            }
            return ruta_archivo;
        }
    }
}
