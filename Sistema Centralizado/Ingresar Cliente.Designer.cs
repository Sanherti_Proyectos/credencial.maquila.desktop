﻿namespace Sistema_Centralizado
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.txtNOMBRE = new System.Windows.Forms.TextBox();
            this.tsPrincipal = new System.Windows.Forms.ToolStrip();
            this.tsbAgregarDB = new System.Windows.Forms.ToolStripButton();
            this.tsbAgregarAPP = new System.Windows.Forms.ToolStripButton();
            this.tsbGuardarDatos = new System.Windows.Forms.ToolStripButton();
            this.tsPrincipal.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(12, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre";
            // 
            // txtNOMBRE
            // 
            this.txtNOMBRE.Location = new System.Drawing.Point(12, 101);
            this.txtNOMBRE.Name = "txtNOMBRE";
            this.txtNOMBRE.Size = new System.Drawing.Size(286, 20);
            this.txtNOMBRE.TabIndex = 1;
            this.txtNOMBRE.TextChanged += new System.EventHandler(this.txtNOMBRE_TextChanged);
            // 
            // tsPrincipal
            // 
            this.tsPrincipal.BackColor = System.Drawing.SystemColors.HotTrack;
            this.tsPrincipal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbAgregarDB,
            this.tsbAgregarAPP,
            this.tsbGuardarDatos});
            this.tsPrincipal.Location = new System.Drawing.Point(0, 0);
            this.tsPrincipal.Name = "tsPrincipal";
            this.tsPrincipal.Size = new System.Drawing.Size(389, 72);
            this.tsPrincipal.TabIndex = 70;
            this.tsPrincipal.Text = "toolStrip1";
            // 
            // tsbAgregarDB
            // 
            this.tsbAgregarDB.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsbAgregarDB.Image = ((System.Drawing.Image)(resources.GetObject("tsbAgregarDB.Image")));
            this.tsbAgregarDB.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbAgregarDB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAgregarDB.Name = "tsbAgregarDB";
            this.tsbAgregarDB.Size = new System.Drawing.Size(87, 69);
            this.tsbAgregarDB.Text = "AGREGAR DB";
            this.tsbAgregarDB.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbAgregarDB.Click += new System.EventHandler(this.tsbAgregarDB_Click);
            // 
            // tsbAgregarAPP
            // 
            this.tsbAgregarAPP.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsbAgregarAPP.Image = ((System.Drawing.Image)(resources.GetObject("tsbAgregarAPP.Image")));
            this.tsbAgregarAPP.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbAgregarAPP.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAgregarAPP.Name = "tsbAgregarAPP";
            this.tsbAgregarAPP.Size = new System.Drawing.Size(139, 69);
            this.tsbAgregarAPP.Text = "AGREGAR APP CONFIG";
            this.tsbAgregarAPP.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tsbGuardarDatos
            // 
            this.tsbGuardarDatos.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsbGuardarDatos.Image = ((System.Drawing.Image)(resources.GetObject("tsbGuardarDatos.Image")));
            this.tsbGuardarDatos.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbGuardarDatos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbGuardarDatos.Name = "tsbGuardarDatos";
            this.tsbGuardarDatos.Size = new System.Drawing.Size(112, 69);
            this.tsbGuardarDatos.Text = "GUARDAR DATOS";
            this.tsbGuardarDatos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(389, 125);
            this.Controls.Add(this.tsPrincipal);
            this.Controls.Add(this.txtNOMBRE);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Ingresar Cliente";
            this.tsPrincipal.ResumeLayout(false);
            this.tsPrincipal.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNOMBRE;
        private System.Windows.Forms.ToolStrip tsPrincipal;
        private System.Windows.Forms.ToolStripButton tsbAgregarDB;
        private System.Windows.Forms.ToolStripButton tsbAgregarAPP;
        private System.Windows.Forms.ToolStripButton tsbGuardarDatos;
    }
}