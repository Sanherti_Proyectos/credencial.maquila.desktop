﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sistema_Centralizado
{
    class Query : BD
    {
        #region INSTRUCCIONES SQL
        #region TABLAS

        public string ALUMNO ="ALUMNO";
        public string CLIENTE = "CLIENTE";
        public string CONFIGURACION = "CONFIGURACION";

        #endregion

        #region PALABRAS RESERVADAS SQL
        public string INSERT_INTO = "INSERT INTO";
        public string SELECT = "SELECT";
        public string FROM = "FROM";
        public string VALUES = "VALUES";
        public string UPDATE = "UPDATE";
        public string SET = "SET";
        public string igual = " = ";
        public string AND = "AND";
        public string ORDEN_BY = "ORDER BY";
        public string ASC = "ASC";
        public string WHERE = "WHERE";
        public string MAX_ID = "MAX";
        public string LIKE = "LIKE";

        public string abrir_parentesis = "(";
        public string cerrar_parentesis = ")";
        public string c = "'";
        public string cc = ",";
        public string ccc = "','";
        #endregion
        #endregion

        #region ATRIBUTOS

        #region ALUMNO

        public string ID_a = "id";
        public string variable1_a = "variable1";
        public string variable2_a = "variable2";
        public string variable3_a = "variable3";
        public string variable4_a = "variable4";
        public string variable5_a = "variable5";
        public string variable6_a = "variable6";
        public string variable7_a = "variable7";
        public string variable8_a = "variable8";
        public string variable9_a = "variable9";
        public string variable10_a = "variable10";
        public string variable11_a = "variable11";
        public string variable12_a = "variable12";

        public string alumno_atributos_insertar = "(`nombre`,`apellido`,`fecha_nacimiento`,`domicilio`,`tutor`,`tutor_2`,`alergia`,`tipo_sangre`)";
        public string alumno_atributos_seleccionar = "`id_alumno`,`nombre`,`apellido`,`domicilio`,`tutor`,`tutor_2`,`alergia`,`tipo_sangre`";

        #endregion

        //#region CLIENTE
        //public string cliente_atributos_insertar = "(`nombre`,`stringconnection`,`posicion`,`rotacion`,`tamaño_fuente`)";
        //public string cliente_atributos_seleccionar = "`id_cliente`,`nombre`,`stringconnection`,`posicion`,`rotacion`,`tamaño_fuente`";

        //public string id_cliente_a = "id_cliente";
        //public string nombre_a = "nombre";
        //public string stringconnection_a = "stringconnection";
        //public string posicion_a = "posicion";
        //public string rotacion_a = "rotacion";
        //public string tamaño_fuente_a = "tamaña_fuente";
        //public string parametros_a = "parametros";
        //#endregion

        #region CLIENTE CONFIGURACION
        public string cliente_info_atributos_insertar = "(`nombre`,`sector_laboral`,`numero_contacto`";
        public string config_cliente_atributos_insertar = "(`nombre_cliente`,`nombre_plantilla`,`nombre_label`,`posicion_label`,`tamaño_fuente`,`rotacion_fuente`,`lado`,`rotacion_plantilla`,`posicion_picturebox`)";
        public string cliente_info_atributos_seleccionar = "(`id_configuracion`,`nombre_cliente`,`nombre_plantilla`,`posicion_label`,`tamaño_fuente`,`rotacion_fuente`,`lado`,`rotacion_plantilla`,`posicion_picturebox`,`nombre_label`)";

        public string id_cliente_info_a = "id_cliente";
        public string nombre_cliente_a = "nombre_cliente";
        public string nombre_plantilla_a = "nombre_plantilla";
        public string posicion_a = "posicion";
        public string tamaño_fuente_a = "tamaño_fuente";
        public string rotacion_fuente_a = "rotacion_fuente";
        public string lado_a = "lado";
        public string rotacion_plantilla_a = "nombre_columna";
        public string posicion_picturebox_a = "posicion_picturebox";
        public string nombre_label_a = "nombre_label";

        public string numero_plantilla_a = "";
        public string nombre_columna_a = "";

        public string id_alumno_a = "id_alumno";
        public string cantidad_impresiones_a = "cantidad_impresiones";
        public string fecha_impresion_a = "fecha_impresion";
        #endregion

        #endregion

        #region METODOS
        public string queryInsertar(string tabla, string atributos, string valores)
        {
            return $"{INSERT_INTO} {tabla} {atributos} {VALUES} {valores}";
        }
        
        public string queryConsulta(string atributo, string tabla, string condicion)
        {
            return $"{SELECT} {atributo} {FROM} {tabla} {condicion}";
        }

        public string queryActualizar(string tabla, string valores, string condicion)
        {
            return $"{UPDATE} {tabla} {SET} {valores} {condicion}";
        }

        public string maxId(string atributo)
        {
            return MAX_ID + abrir_parentesis + atributo + cerrar_parentesis;
        }
        #endregion
    }
}
