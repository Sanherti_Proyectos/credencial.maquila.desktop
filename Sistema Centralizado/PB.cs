using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace Sistema_Centralizado
{
    public class PB
    {
        public PictureBox pictureBox, pictureBoxActual;
        //Labels label = new Labels();
        public List<PictureBox> listaPictureBox = new List<PictureBox>();
        int ubicacion_pb_X = 50;
        int ubicacion_pb_Y = 50;
        Point mousePosicion;
        public bool clicPb { get; set; } = false;

        public void generarPictureBox(PictureBox pb)
        {
            pictureBoxActual = pb;

            pictureBox = new PictureBox();
            pictureBox.Location = new Point(ubicacion_pb_X, ubicacion_pb_Y);
            pictureBox.Size = new Size(86, 103);
            pictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox.BackColor = Color.Aqua;
            pictureBox.BringToFront();

            pictureBox.MouseDown += new MouseEventHandler(pictureBox_MouseDown);
            pictureBox.MouseMove += new MouseEventHandler(pictureBox_MouseMove);
            pictureBox.Click += new EventHandler(pictureBox_Click);
            pictureBox.MouseDoubleClick += new MouseEventHandler(pictureBox_DoubleClick);
            pictureBox.KeyPress += new KeyPressEventHandler(pictureBox_KeyPress);

            pictureBoxActual.Controls.Add(pictureBox);
            listaPictureBox.Add(pictureBox);
            

        }

        //private void pictureBox_Click(object sender, EventArgs e)
        //{
        //    pbActual = sender as PictureBox;

        //    string nombrepicturebox = pbActual.Name;
        //}

        private void pictureBox_Click(object sender, EventArgs e)
        {
            pictureBoxActual = sender as PictureBox;
            clicPb = true;
        }

        private void pictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            mousePosicion = e.Location;
        }

        private void pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                int dx = e.X - mousePosicion.X;
                int dy = e.Y - mousePosicion.Y;
                pictureBoxActual = sender as PictureBox;

                pictureBoxActual.Location = new Point(pictureBoxActual.Left + dx, pictureBoxActual.Top + dy);
                pictureBoxActual.BringToFront();
            }

        }
 
        public void pictureBox_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                pictureBoxActual.Image = Image.FromFile(abrirFoto());
            }
            catch (Exception exc)
            {
                MessageBox.Show("No se pudo cargar la imagen");
            }
        }

        public void pictureBoxEliminar(Control pb)
        {
            try
            {
                pb.Controls.Remove(listaPictureBox[indexPictureBoxActual()]);
                listaPictureBox.Remove(listaPictureBox[indexPictureBoxActual()]);
            }
            catch (Exception ex)
            {

            }
        }

        public void pictureBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.C)
            {
                pictureBoxActual.Controls.Remove(pictureBoxActual);
            }
        }

        //private void pbFrente_Click(object sender, EventArgs e)
        //{
        //    //label.etiquetaActual = null;
        //    pictureBoxActual = null;
        //}

        //private void pbVuelta_Click(object sender, EventArgs e)
        //{
        //    //label.etiquetaActual = null;
        //    pictureBoxActual = null;
        //}

        public int indexPictureBoxActual()
        {
            return listaPictureBox.IndexOf(pictureBoxActual);
        }

        public String abrirFoto()
        {
            Stream stream = null;
            OpenFileDialog ventana_imagen = new OpenFileDialog();
            string ruta_archivo = "";

            ventana_imagen.InitialDirectory = "C:\\Imagenes\\";
            ventana_imagen.Filter = "Tipo de imagenes .jpeg .png .jpg | *.jpeg;*.png;*.jpg";

            if (ventana_imagen.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((stream = ventana_imagen.OpenFile()) != null)
                    {
                        using (stream)
                        {
                            ruta_archivo = ventana_imagen.FileName;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: No se pudo abrir el archivo " + ex.Message);
                }
            }

            return ruta_archivo;
        }
    }
}
