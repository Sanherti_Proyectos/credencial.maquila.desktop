﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sistema_Centralizado
{
    class Contador : Query
    {
        public string id_alumno = "";
        public string fecha_impresion = "";
        public string cantidad_impresiones = "";

        public string tabla = "";
        public string atributos_insertar = "";
        public string atributos_seleccionar = "";

        public Contador()
        {
            this.id_alumno = id_alumno_a;
            this.fecha_impresion = DateTime.Now.ToString();
            this.cantidad_impresiones = cantidad_impresiones_a + 1;

            this.tabla = ALUMNO;
            this.atributos_insertar = alumno_atributos_insertar;
            this.atributos_seleccionar = alumno_atributos_seleccionar;

        }

        //public Contador()
        //{
        //    tabla = ALUMNO;
        //    atributos_insertar = alumno_atributos_insertar;
        //    atributos_seleccionar = alumno_atributos_seleccionar;
        //}


        #region VALORES

        public string valores()
        {
            return
            $"{abrir_parentesis}" +
            $"{c}" +
            $"{fecha_impresion.ToUpper()} {ccc}" +
            $"{cantidad_impresiones}" +
            $"{c}" +
            $"{cerrar_parentesis} ";
        }

        public string valoresUpdate()
        {
            return
            $"{fecha_impresion_a} {igual} {c}{fecha_impresion}{c} {cc}" +
            $"{cantidad_impresiones_a} {igual} {c}{cantidad_impresiones}{c}";
        }

        #endregion

        #region METODOS
           
        public bool insertarContador()
        {
            return insertar(queryInsertar(tabla, atributos_insertar, valores()));
        }

        //public bool acutualizarContador()
        //{
        //    return insertar(queryActualizar(ALUMNO, valoresUpdate(), WHERE + id_alumno_a + igual + c + this.id_alumno + c));
        //}

        #endregion
    }
}
