﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


namespace Sistema_Centralizado
{
    class Configuracion_Cliente : Query
    {
        Cliente cliente;
        Labels label = new Labels();
        List<string> param_list = new List<string>();
        StringBuilder temporal = new StringBuilder();
        PictureBox pbIzq = new PictureBox();
        List<string> configuracion = new List<string>();
        string[] configuracion_cliente = new string[20];

        string parametro = "";
        int contador = 0;

        private List<int> numero_plantilla = new List<int>();
        private string nombre_columna = "";

        private string id_configuracion = "";
        private string nombre_cliente = "";
        private string nombre_plantilla = "";
        private string posicion_label = "";
        private string tamaño_fuente = "";
        private string rotacion_fuente = "";
        private string lado = "";
        private string rotacion_plantilla = "";
        private string posicion_pictureBox = "";
        private string nombre_label = "";

        public string path_frente { get; set; }
        public string path_vuelta { get; set; }



        string tabla = "";
        public string atributos_insertar = "";
        public string atributos_seleccionar = "";

        public Configuracion_Cliente()
        {
            atributos_insertar = config_cliente_atributos_insertar;
            tabla = CONFIGURACION;
        }

        public bool insertar()
        {
            return insertar(queryInsertar(tabla, atributos_insertar, valores()));
        }

        public void nombrePlantillas(string nombreCliente)
        {

        }

        public void llenarComboBoxConfiguracion(string nombreCliente, ComboBox secundario, int lado, ref bool entra)
        {

            Array.Clear(configuracion_cliente, 0, configuracion_cliente.Length);

            string nombrePlantilla = "nombre_plantilla";

            string condicion = "WHERE nombre_cliente = " + c + nombreCliente + c + " and lado = " + lado;

            llenarComboBox(secundario, queryConsulta(nombrePlantilla, tabla, condicion), nombrePlantilla, tabla, ref entra);

        }

        public void numeroPlantillas(string path_frente, string path_vuelta)
        {
            int archivos_frente = numeroDeArchivos(path_frente);
            int archivos_vuelta = numeroDeArchivos(path_vuelta);

            Numero_plantilla.Add(archivos_frente);
            Numero_plantilla.Add(archivos_vuelta);
        }

        public int numeroDeArchivos(string path)
        {
            if (Directory.Exists(path))
            {
                string[] archivos = Directory.GetFiles(@path, "*.PNG");

                int cantidad = archivos.Length;

                return cantidad;

            }
            else
            {
                return 0;
            }
        }


        #region Valores & ValoresUpdate
        public string valores()
        {
            return $"{abrir_parentesis}" +
                    $"{c}" +
                    $"{nombre_cliente} {ccc}" +
                    $"{nombre_plantilla} {ccc}" +
                    $"{nombre_label} {ccc}" +
                    $"{posicion_label}{ccc}" +
                    $"{tamaño_fuente}{ccc}" +
                    $"{rotacion_fuente}{ccc}" +
                    $"{lado} {ccc}" +
                    $"{rotacion_plantilla}{ccc}" +
                    $"{posicion_pictureBox}" +
                    $"{c}" +
                  $"{cerrar_parentesis}";
        }

        public string valoresUpdate()
        {
            return
            $"{nombre_cliente} {igual} {c} {Nombre_cliente}" +
            $"{nombre_plantilla} {igual} {c} {Nombre_plantilla}{c}" +
            $"{posicion_label} {igual} {c} {Posicion_label}{c} {cc}" +
            $"{tamaño_fuente} {igual} {c} {Tamaño_fuente}{c} {cc}" +
            $"{rotacion_fuente} {igual} {c} {Rotacion_fuente}{c} {cc}" +
            $"{lado} {igual} {c} {Lado}{c} {cc}" +
            $"{rotacion_plantilla} {igual} {c} {Rotacion_plantilla}{c} {cc}" +
            $"{posicion_pictureBox} {igual} {c} {Posicion_pictureBox}{c} {cc}" +
            $"{nombre_label} {igual} {c} {Nombre_label}{c}";
        }

        #endregion


        #region SET AND GET
        public List<int> Numero_plantilla
        {
            get
            {
                return numero_plantilla;
            }

            set
            {
                numero_plantilla = value;
            }
        }

        public string Nombre_columna
        {
            get
            {
                return nombre_columna;
            }

            set
            {
                nombre_columna = value;
            }
        }

        public string Id_configuracion
        {
            get
            {
                return id_configuracion;
            }

            set
            {
                id_configuracion = value;
            }
        }

        public string Nombre_cliente
        {
            get
            {
                return nombre_cliente;
            }

            set
            {
                nombre_cliente = value;
            }
        }

        public string Nombre_plantilla
        {
            get
            {
                return nombre_plantilla;
            }

            set
            {
                nombre_plantilla = value;
            }
        }

        public string Posicion_label
        {
            get
            {
                return posicion_label;
            }

            set
            {
                posicion_label = value;
            }
        }

        public string Tamaño_fuente
        {
            get
            {
                return tamaño_fuente;
            }

            set
            {
                tamaño_fuente = value;
            }
        }

        public string Rotacion_fuente
        {
            get
            {
                return rotacion_fuente;
            }

            set
            {
                rotacion_fuente = value;
            }
        }

        public string Lado
        {
            get
            {
                return lado;
            }

            set
            {
                lado = value;
            }
        }

        public string Rotacion_plantilla
        {
            get
            {
                return rotacion_plantilla;
            }

            set
            {
                rotacion_plantilla = value;
            }
        }

        public string Posicion_pictureBox
        {
            get
            {
                return posicion_pictureBox;
            }

            set
            {
                posicion_pictureBox = value;
            }
        }

        public string Nombre_label
        {
            get
            {
                return nombre_label;
            }

            set
            {
                nombre_label = value;
            }
        }


        #endregion
    }
}

