﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;
using System.IO;
using System.Drawing;
using System.Windows.Forms;

namespace Sistema_Centralizado
{
    class BD
    {
        //string parametrosConexion = System.Configuration.ConfigurationManager.ConnectionStrings["db"].ConnectionString;
        public string nombreBD;
        string parametrosConexion;
        SQLiteConnection conexionBD;
        SQLiteCommand queryCommand;
        public SQLiteDataAdapter adaptador;
        SQLiteDataReader reader;
        SQLiteParameter picture;
        SQLiteParameter picture2;
        public DataSet obtenerDatos;

        //public BD(string nombreBD)
        //{
        //    this.parametrosConexion = $"Data Source = {nombreBD}; Version=3; New=False;Compress=True;";
        //}

        public bool conexionAbrir()
        {
            string parametrosConexion = $"Data Source = {nombreBD}; Version=3; New=False;Compress=True;";

            conexionBD = new SQLiteConnection(parametrosConexion);
            ConnectionState estadoConexion;
            try
            {
                conexionBD.Open();
                estadoConexion = conexionBD.State;
                return true;
            }
            catch (SQLiteException)
            {
                return false;
            }
        }



        public bool insertar(string sqliteInsertar)
        {
            int filasafectadas = 0;
            conexionAbrir();

            queryCommand = new SQLiteCommand(sqliteInsertar, conexionBD);
            //filasafectadas = queryCommand.ExecuteNonQuery();

            using (SQLiteCommand cmd = new SQLiteCommand(sqliteInsertar, conexionBD))
            {
                filasafectadas = queryCommand.ExecuteNonQuery();
            }

                conexionCerrar();

            if (filasafectadas > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public DataSet consulta(string queryConsulta, string tabla)
        {
            conexionAbrir();

            obtenerDatos = new DataSet();
            //obtenerDatos.Reset();

            adaptador = new SQLiteDataAdapter(queryConsulta, conexionBD);

            adaptador.Fill(obtenerDatos, tabla);

            conexionCerrar();

            return obtenerDatos;
        }

        public void llenarComboBox(ComboBox llenarCombo, string sqlLlenarComboBox, string columna, string tabla, ref bool entra)
        {
            obtenerDatos = consulta(sqlLlenarComboBox, tabla);

            entra = false;
            llenarCombo.DataSource = obtenerDatos.Tables[tabla];
            llenarCombo.DisplayMember = columna;
            llenarCombo.ValueMember = columna;
            llenarCombo.SelectedIndex = -1;
            entra = true;
        }

        public void llenarListBox(ListBox llenarlista, string queryLlenarListView, string columna, string tabla, ref bool entra)
        {
            //obtenerDatos = new DataSet();
            //obtenerDatos.Reset();
            obtenerDatos = consulta(queryLlenarListView, tabla);

            //adaptador.Fill(obtenerDatos, tabla);

            entra = false;
            llenarlista.DataSource = obtenerDatos.Tables[tabla];
            llenarlista.DisplayMember = columna;
            //llenarCombo.SelectedIndex = -1;
            entra = true;
        }

        private void conexionCerrar()
        {
            if (conexionBD.State == ConnectionState.Open)
            {
                conexionBD.Close();
            }
        }

        public bool guardarImagen(Image imagen, /*System.Windows.Forms.PictureBox pbImagen2*/ int id)
        {
            if (imagen != null)
            {
                int filasAfectadas = 0;

                picture = new SQLiteParameter("@picture", SqlDbType.Image);
                //picture2 = new SQLiteParameter("@picture2", SqlDbType.Image);

                MemoryStream ms = new MemoryStream();
                MemoryStream ms2 = new MemoryStream();

                imagen.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                //pbImagen2.Image.Save(ms2, System.Drawing.Imaging.ImageFormat.Bmp);


                byte[] imagenByte = ms.GetBuffer();
                //byte[] imagenByte2 = ms2.GetBuffer();

                ms.Close();
                conexionAbrir();
                queryCommand = new SQLiteCommand(conexionBD);
                queryCommand.Parameters.Clear();
                queryCommand.Parameters.AddWithValue("@picture", imagenByte);
                //queryCommand.Parameters.AddWithValue("@picture2", imagenByte2);
                queryCommand.CommandText = "UPDATE `IMAGEN` SET imagen  = @picture WHERE `id_imagen` = " + id; //"INSERT INTO `IMAGEN`(`id_imagen`,`imagen`) VALUES (" + id + ", @picture)";
                /*"UPDATE `Visitante` SET `fotorostro` = (@picture) WHERE `idvisitante` = " + idVisitante + "";*/
                filasAfectadas = queryCommand.ExecuteNonQuery();
                conexionCerrar();

                if (filasAfectadas > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }

        public bool actualizar(string tabla, string datos, string condicion)
        {
            conexionAbrir();

            int filasAfectadas = 0;
            string queryActualizar = "UPDATE " + tabla + " SET " + datos + " WHERE " + condicion;

            queryCommand = new SQLiteCommand(queryActualizar, conexionBD);

            filasAfectadas = queryCommand.ExecuteNonQuery();

            conexionCerrar();

            if (filasAfectadas > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void llenarDgv(DataGridView dgvLlenar, string query, string tabla)
        {
            //obtenerDatos = new DataSet();
            //obtenerDatos.Reset();

            obtenerDatos = consulta(query, tabla);
            //adaptador.Fill(obtenerDatos, tabla);

            dgvLlenar.DataSource = obtenerDatos.Tables[0];
        }
    }
}
