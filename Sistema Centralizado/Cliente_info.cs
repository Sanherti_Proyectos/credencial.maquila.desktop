﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


namespace Sistema_Centralizado
{
    class Cliente_info : Query
    {
        Labels label = new Labels();
        List<string> param_list = new List<string>();
        StringBuilder temporal = new StringBuilder();
        //public PB pbLocal { get; set; }
        //PictureBox pbLocal = new PictureBox();
        System.Windows.Forms.PictureBox pbIzq = new System.Windows.Forms.PictureBox();

        string parametro = "";
        int contador = 0;

        private string id_cliente = "";
        private string nombre = "";
        private string posicion = "";
        private string rotacion = "";
        private string tamaño_fuente = "";
        private string rotacion_plantilla = "";
        private string numero_plantilla = "";
        private string nombre_columna = "";
       

        public string path_frente { get; set; }
        public string path_vuelta { get; set; }

        string tabla = "";
        string nombre_cliente = "";
        public string atributos_insertar = "";
        public string atributos_seleccionar = "";
        private string queryInsertarContador = "";

        public Cliente_info()
        {
            //path_frente = $@"plantilla\{Nombre}\frente";
            //path_vuelta = $@"plantilla\{Nombre}\vuelta";

            tabla = "CLIENTE";
            atributos_insertar = cliente_info_atributos_insertar;
            atributos_seleccionar = cliente_info_atributos_seleccionar;
        }

        public bool insertarCliente()
        {
            return insertar(queryInsertar(tabla, atributos_insertar, valores()));
        }

        public bool cargarCliente(string nombre_cliente)
        {
            try
            {
                obtenerDatos = consulta(queryConsulta(atributos_seleccionar, tabla, $"{WHERE} {nombre_cliente_a} {igual} {c}{nombre_cliente}{c}"), tabla);

                Id_cliente = obtenerDatos.Tables[tabla].Rows[0][0].ToString();
                Nombre = obtenerDatos.Tables[tabla].Rows[0][1].ToString();
                Posicion = obtenerDatos.Tables[tabla].Rows[0][2].ToString();
                Rotacion = obtenerDatos.Tables[tabla].Rows[0][3].ToString();
                Tamaño_fuente = obtenerDatos.Tables[tabla].Rows[0][4].ToString();

                return true;
            }
            catch (IndexOutOfRangeException )
            {
                return false;
            }
        }

        

        public bool existe(string nombre_cliente)
        {
            return insertar(queryConsulta(nombre_cliente_a, tabla, $"{WHERE} {nombre_cliente_a} {igual} {c}{nombre_cliente}{c}"));
        }

        public string cadenaNumeroPlantilla(string path_frente, string path_vuelta)
        {
            int archivos_frente = numeroDeArchivos(path_frente);
            int archivos_vuelta = numeroDeArchivos(path_vuelta);

            if (archivos_frente != 0 && archivos_vuelta != 0)
            {
                return $"{archivos_frente}|{archivos_vuelta}";
            }
            else
            {
                return "false";//$"Hay {archivos_frente} archivos en la carpeta frete \n Hay {archivos_vuelta} archivos en la carpeta vuelta";
            }
        }

        public int numeroDeArchivos(string path)
        {
            if (Directory.Exists(path))
            {
                string[] archivos = Directory.GetFiles(@path, "*.PNG");

                int cantidad = archivos.Length;

                return cantidad;
                
            }
            else
            {
                
                return 0;
            }
        }

        public string valores()
        {
            return $"{abrir_parentesis}"+ 
                    $"{c}"+ 
                    $"{Nombre}{ccc}"+ 
                    $"{Posicion}{ccc}"+ 
                    $"{Rotacion}{ccc}"+
                    $"{Tamaño_fuente}{ccc}"+
                    $"{Rotacion_plantilla}{ccc}"+
                    $"{Numero_plantilla}{ccc}" +
                    $"{Nombre_columna}" +
                    $"{c}"+ 
                  $"{cerrar_parentesis}";
        }

        public string ValoresActualizar()
        {
            return $"{nombre_cliente_a} {igual} {c} {Nombre} {c}" +
                   $"{posicion_a} {igual} {c} {Posicion} {c}" +
                   $"{rotacion} {igual} {c} {Rotacion} {c}" +
                   $"{tamaño_fuente_a} {igual} {c} {Tamaño_fuente} {c}"+ 
                   $"{rotacion_plantilla_a} {igual} {c} {Rotacion_plantilla} {c}"+
                   $"{numero_plantilla_a} {igual} {c} {Numero_plantilla} {c}"+
                   $"{nombre_columna_a} {igual} {c} {Nombre_columna} {c}";
        }

        public string obtener_StringParametros(string nombre_cliente)
        {
            obtenerDatos = consulta(queryConsulta(posicion_a, tabla, WHERE + nombre_cliente_a + igual + c + nombre_cliente + c), tabla);
            string parametros = obtenerDatos.Tables[tabla].Rows[0][0].ToString();
            
            return parametros;
        }

        public List<string> dividir(string nombre_cliente)
        {
            string parametros = obtener_StringParametros(nombre_cliente);
            var caracter = parametros.ToCharArray();
            int contador_aux = 0;

            for (int contador=0;contador<caracter.Length;contador++)
            {
                contador_aux++;

                if(caracter[contador]>=48 && caracter[contador]<=57)
                {
                    temporal.Append(caracter[contador]);
                }

                if (caracter[contador] == '-' || caracter[contador] == ',' || contador_aux == caracter.Length)
                {
                    param_list.Add(temporal.ToString());
                    temporal.Clear();
                }
            }
            
            return param_list;
        }

        public List<string> dividir_labelParametros(string nombre_cliente)
        {
            string parametros = obtener_StringParametros(nombre_cliente);
            var caracter = parametros.ToCharArray();
            int contador_aux = 0;
            
            for(contador=0 ; contador < caracter.Length ; contador++)
            {
                contador_aux++;
                
                if(caracter[contador] != '-')
                {
                    temporal.Append(caracter[contador]);
                }

                if(caracter[contador] == '-' || contador_aux==caracter.Length)
                {
                    param_list.Add(temporal.ToString());
                    temporal.Clear();
                }
                
            }

            return param_list;
        }

        

        #region SET AND GET
        public string Id_cliente
        {
            get
            {
                return id_cliente;
            }

            set
            {
                id_cliente = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Posicion
        {
            get
            {
                return posicion;
            }

            set
            {
                posicion = value;
            }
        }

        public string Rotacion
        {
            get
            {
                return rotacion;
            }

            set
            {
                rotacion = value;
            }
        }

        public string Tamaño_fuente
        {
            get
            {
                return tamaño_fuente;
            }

            set
            {
                tamaño_fuente = value;
            }
        }

        public string Rotacion_plantilla
        {
            get
            {
                return rotacion_plantilla;
            }

            set
            {
                rotacion_plantilla = value;
            }
        }

        public string Numero_plantilla
        {
            get
            {
                return numero_plantilla;
            }

            set
            {
                numero_plantilla = value;
            }
        }

        public string Nombre_columna
        {
            get
            {
                return nombre_columna;
            }

            set
            {
                nombre_columna = value;
            }
        }

        #endregion
    }
}
