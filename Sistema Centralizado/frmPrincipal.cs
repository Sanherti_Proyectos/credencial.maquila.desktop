﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
//using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace Sistema_Centralizado
{
    public partial class frmPrincipal : Form
    {
        Configuracion_AppConf config = new Configuracion_AppConf();
        Query Query = new Query();
        PictureBox pictureBoxActual;
        //PictureBox pbActual;
        PictureBoxs picturebox = new PictureBoxs();
        Cliente cliente;
        Cliente_info cliente_informacion;
        RowData rd = new RowData();
        Contador contador = new Contador();
        Configuracion_Cliente configuracion_cliente;

        Labels label = new Labels();
        PB pb = new PB();
        List<string> param_list = new List<string>();
        bool entra = true;
        int rotacion_plantilla = 1;
        string bd_local = "sistema_centralizado.db";
        string id_cellData = "";
        string nombreCliente = "";
        string nombrePlantilla_izquierda = "";
        string nombrePlantilla_derecha = "";
        //string CLIENTE = "CLIENTE";
        

        public frmPrincipal()
        {
            InitializeComponent();
            inicializarEnventosPictureBox();

            cliente = new Cliente() { nombreBD = "sistema_centralizado.db" };
            cliente.llenarComboBoxCliente(cbBasedeDatos, ref entra);

            pictureBoxActual = pbFrente;
        }

        //evento clic que asigna el pictureBox seleccionado a la variable pbActual
        private void pictureBox_Click(object sender, EventArgs e)
        {
            pictureBoxActual = sender as PictureBox; 
            string nombrepicturebox = pictureBoxActual.Name;
            label.etiquetaActual = null;
            pb.pictureBoxActual = null;
           
        }

        //inicializa los eventos para los controles del pictureBox
        private void inicializarEnventosPictureBox()
        {
            pbFrente.Click += new EventHandler(pictureBox_Click);
            pbVuelta.Click += new EventHandler(pictureBox_Click);
        }

        #region Eventos ToolStrip
        //
        private void tsbVerDiseño_Click(object sender, EventArgs e)
        {
            //Tamaño base 528, 554
            if (Size.Width == 528 && Size.Height == 554)
            {
                this.Size = new Size(1051, 554);
            }

            else/*(Size.Width == 1051 && Size.Height == 554)*/
            {
                this.Size = new Size(528, 554);
            }
        }

        public void tsbCambio_Click(object sender, EventArgs e)
        {
            if (rotacion_plantilla == 1)
            {
                //gbDiseñoHorizontal.Location = new Point(541, 90);
                //gbDiseñoVertical.Location = new Point(1025, 90);
                orientacionDiseño(pbFrente, 368, 187, 47, 46);
                orientacionDiseño(pbVuelta, 368, 187, 47, 239);
                comboBox_izq.Location = new Point(20, 20);
                comboBox_der.Location = new Point(300, 20);
                rotacion_plantilla = 0;
                tsbCambio.Text = "Diseño vertical";
                gbPlantilla.Text = "Diseño Horizontal";
            }

            else
            {
                //gbDiseñoHorizontal.Location = new Point(1025, 90);
                //gbDiseñoVertical.Location = new Point(541, 90);
                orientacionDiseño(pbFrente, 230, 369, 6, 58);
                orientacionDiseño(pbVuelta, 230, 369, 242, 58);
                comboBox_der.Location = new Point(300, 27);
                comboBox_izq.Location = new Point(18, 27);
                rotacion_plantilla = 1;
                tsbCambio.Text = "Diseño horizontal";
                gbPlantilla.Text = "Diseño Vertical";
            }
        }

        public void orientacionDiseño(System.Windows.Forms.PictureBox pb, int width, int height, int x, int y)
        {
            pb.Width = width;
            pb.Height = height;
            pb.Location = new Point(x, y);
            
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            //config.openfile();
            //config.prueba();
            //config.modificar_appConfig();
            //String path = config.openfile();
            //String conexion = config.Obtener_cadenaConfig(path);
            //config.agregar_StringConfig();
            //config.obtener_currente();
            //config.master();
        }

        private void tsbSubirFoto_Click(object sender, EventArgs e)
        {
            OpenFileDialog getImage = new OpenFileDialog();
            getImage.InitialDirectory = "C:\\fotos\\";
            getImage.Filter = "Archivos de Imagen (*.jpg)(*.jpge)|*.jpg;*.jpge";
            getImage.FileName = "";

            if (getImage.ShowDialog() == DialogResult.OK)
            {
                //pbImagen.SizeMode = PictureBoxSizeMode.StretchImage;
                /*pbImagen.ImageLocation = getImage.FileName;
                 pbImagen.Visible = true;
                 pbVideo.Visible = false;
                */
            }
        }

        private void tsbInsertarRegistro_Click(object sender, EventArgs e)
        {
            string nombre_cliente = cbBasedeDatos.Text.ToUpper().Trim();

            cliente_informacion = new Cliente_info()
            {
                Nombre = cbBasedeDatos.Text,
                
                nombreBD = bd_local,
                Posicion = label.stringPotitionLabel(),
                Rotacion = label.stringRotationLabel(),
                Tamaño_fuente = label.stringFontSizeLabel(),
                Rotacion_plantilla = this.rotacion_plantilla.ToString(),
               
            };
            cliente_informacion.path_frente = $@"plantillas\{cliente_informacion.Nombre}\frente";
            cliente_informacion.path_vuelta = $@"plantillas\{cliente_informacion.Nombre}\vuelta";
            cliente_informacion.Numero_plantilla = cliente_informacion.cadenaNumeroPlantilla(cliente_informacion.path_frente, cliente_informacion.path_vuelta);

            if (nombre_cliente != "")
            {
                if (!cliente_informacion.existe(nombre_cliente))
                {
                    if (cliente_informacion.insertarCliente())
                    {
                        MessageBox.Show("Registro exitoso");
                    }
                    else
                    {
                        MessageBox.Show("No se realizo el registro intente de nuevo");
                    }
                }
                else
                {
                    MessageBox.Show("Ya existe un registro con ese nombre");
                }
            }
            else
            {
                MessageBox.Show("El campo cliente no puede estar vacio");
            }   
        }
        #endregion

        private void cbBasedeDatos_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (entra)
            {
                nombreCliente = cbBasedeDatos.Text;

                //Se crea una nueva instancia de cliente con el nombre de la base de datos
                //del cliente seleccionado en el comboBox
                cliente = new Cliente() { nombreBD = $"{nombreCliente}.db", tabla = cliente.ALUMNO };
                cliente_informacion = new Cliente_info() {nombreBD = bd_local };
                configuracion_cliente = new Configuracion_Cliente() { nombreBD = "sistema_centralizado.db" };

                //cliente_informacion.cargarCliente(nombreCliente);//cbBasedeDatos.Text);

                cliente.llenarDgvBDcliente(dgvBDcliente);

                configuracion_cliente.llenarComboBoxConfiguracion(nombreCliente, comboBox_izq, 0, ref entra);
                configuracion_cliente.llenarComboBoxConfiguracion(nombreCliente, comboBox_der, 1, ref entra);
                
                obtenerDiseño(nombreCliente);
                
            }
        }

        public string nombre_Cliente()
        {
            return nombreCliente;
        }

        public string obtenerNombrePlantilla_Izquierda()
        {
            return nombrePlantilla_izquierda;
        }

        public string obtenerNombrePlantilla_Derecha()
        {
            return nombrePlantilla_derecha;
        }

        public void obtenerDiseño(string nombre_cliente)
        {
            List<string> x_izquierdo = new List<string>();
            List<string> y_izquierdo = new List<string>();

            List<string> x_derecho = new List<string>();
            List<string> y_derecho = new List<string>();

            if (cliente_informacion.Posicion != "")
            {
                x_izquierdo = label.posiciones("x", "izquierdo", cliente_informacion.Posicion);//dividir(nombre_cliente);
                y_izquierdo = label.posiciones("y", "izquierdo", cliente_informacion.Posicion);

                x_derecho = label.posiciones("x", "derecho", cliente_informacion.Posicion);
                y_derecho = label.posiciones("y", "derecho", cliente_informacion.Posicion);
            }

            rd.inicializarNombreColumna(dgvBDcliente);

            label.reacomodo_label(rd.ColumnasDGV, pbFrente, x_izquierdo, y_izquierdo);

            //cargar_plantilla(nombre_cliente);
            //cargarPlantilla(nombrePlantilla_izquierda, 0);
            //cargarPlantilla(nombrePlantilla_derecha, 1);
        }

        public void cargarPlantilla(string nombrePlantilla, int lado)
        {
            string nombre_cliente = nombre_Cliente();

            if(lado == 0)
            {
                string frente = Directory.GetCurrentDirectory() + "\\plantillas\\" + nombre_cliente + "\\frente\\" + nombrePlantilla + ".png";

                try
                {
                    pbFrente.Image = Image.FromFile(frente);
                    pbFrente.SizeMode = PictureBoxSizeMode.StretchImage;
                }
                catch
                {
                    MessageBox.Show("No se ha podido cargar la plantilla frontal");
                }
            }
            
            else
            {
                string vuelta = Directory.GetCurrentDirectory() + "\\plantillas\\" + nombre_cliente + "\\vuelta\\" + nombrePlantilla + ".png";

                try
                {
                    pbVuelta.Image = Image.FromFile(vuelta);
                    pbVuelta.SizeMode = PictureBoxSizeMode.StretchImage;
                }
                catch
                {
                    MessageBox.Show("No se ha podido cargar la plantilla trasera");
                }
            }
            
        }

        //public void cargar_plantilla(string nombre_cliente)
        //{
        //    List<System.IO.FileInfo> plantillas_frente = new List<System.IO.FileInfo>();
        //    System.IO.FileInfo[] array_frente = { };
        //    System.IO.FileInfo[] array_vuelta = { };

        //    string frente = Directory.GetCurrentDirectory() + "\\plantillas\\" + nombre_cliente + "\\frente\\";
        //    string vuelta = Directory.GetCurrentDirectory() + "\\plantillas\\" + nombre_cliente + "\\vuelta\\";

        //    DirectoryInfo directorio_frente = new DirectoryInfo(Directory.GetCurrentDirectory()+"\\plantillas\\" + nombre_cliente + "\\frente");
        //    DirectoryInfo directorio_vuelta = new DirectoryInfo(vuelta);

        //    array_frente = directorio_frente.GetFiles().ToArray();
        //    array_vuelta = directorio_vuelta.GetFiles().ToArray();
            
        //    try
        //    {
        //        pbFrente.Image = Image.FromFile(frente + array_frente[0].ToString());
        //        pbFrente.SizeMode = PictureBoxSizeMode.StretchImage;
        //        pbVuelta.Image = Image.FromFile(vuelta + array_vuelta[0].ToString());
        //        pbVuelta.SizeMode = PictureBoxSizeMode.StretchImage;
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show("No se ha podido cargar la imagen de la plantilla");
        //    }

        //}

        //public void identificador_plantilla()
        //{

        //}

        private void tsbInsertarLabel_Click(object sender, EventArgs e)
        {
            string nombreLabel = "";
            int rotacion = 0;
            int incrementoPosicion = 0;

            nombreLabel = Interaction.InputBox("Intruduzca el nombre de referencia para esta etiqueta",
                                               "Nombre label", "", -1, -1);

            if (nombreLabel != "")
            {
                label.generarLabel(pictureBoxActual, nombreLabel, "115", Convert.ToString(0 + (25 * incrementoPosicion)), rotacion);
            }
            else
            {
                MessageBox.Show("Falto poner nombre al Label", caption: "AVISO", buttons: MessageBoxButtons.OK, icon: MessageBoxIcon.Warning);
            }
            
        }

        private void tsbEliminarEtiqueta_Click(object sender, EventArgs e)
        {
            if (label.clicLabel)
            {
                label.etiquetaDelete(label.listaLabel[label.indexEtiquetaActual()].Parent);
                label.clicLabel = false;
            }

            else if (pb.clicPb)
            {
                pb.pictureBoxEliminar(pb.listaPictureBox[pb.indexPictureBoxActual()].Parent);
                pb.clicPb = false;
            }

            else
                MessageBox.Show("No hay ningun elemento seleccionado");
        }
        
        private void tsbNuevaImagen_Click(object sender, EventArgs e)
        {
            pb.generarPictureBox(pictureBoxActual);
        }

        #region Data Grid View

    private void dgvBDcliente_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            rd.nombreBD = $"{cbBasedeDatos.Text}.db";
            rd.modificar();
        }
        #endregion

        private void Cambio_Click(object sender, EventArgs e)
        {
            cambiarElemento();
        }

        private void tsbImprimir_Click(object sender, EventArgs e)
        {
            
            if( id_cellData !="")
            {
                DialogResult respuesta = MessageBox.Show("¿Deseas guardar los cambios de impresion?", "AVISO!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);


                if (respuesta == DialogResult.Yes)
                {
                    //rd.registrarImpresion();
                    cliente.insertarContador(id_cellData);
                    id_cellData = "";
                }
            }
            
            else
            {
                MessageBox.Show("necesitas seleccionar una fila");
            }

        }

        #region metodos externos

        public Boolean cambiarElemento()
        {
            int index_label = label.indexEtiquetaActual();
            int index_Picturebox = pb.indexPictureBoxActual();

            try
            {

                if(label.clicLabel)
                {
                    if (label.listaLabel[index_label].Parent == pbFrente)
                    {
                        label.listaLabel[index_label].Parent = pbVuelta;
                        return true;
                    }
                    else
                    {
                        label.listaLabel[index_label].Parent = pbFrente;
                        return true;
                    }
                }

                if(pb.clicPb)
                {
                    if (pb.listaPictureBox[index_Picturebox].Parent == pbFrente)
                    {
                        pb.listaPictureBox[index_Picturebox].Parent = pbVuelta;
                        return true;
                    }
                    else
                    {
                        pb.listaPictureBox[index_Picturebox].Parent = pbFrente;
                        return true;
                    }
                }
                
            }
            catch
            {
                MessageBox.Show("no hay elemento seleccionado");
            }

            return false;
        }

        private void dgvBDcliente_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView DGV = dgvBDcliente;
            id_cellData = rd.inicializarRowData(DGV, e);
            
        }

        private void Agregar_PictureBox(object sender, EventArgs e)
        {
            pb.generarPictureBox(pictureBoxActual);
        }

        private void comboBox_izq_SelectedIndexChanged(object sender, EventArgs e)
        {
            nombrePlantilla_izquierda = comboBox_izq.Text;

            if(entra)
            {
                cargarPlantilla(nombrePlantilla_izquierda, 0);
            }
            
         }

        private void comboBox_der_SelectedIndexChanged(object sender, EventArgs e)
        {
            nombrePlantilla_derecha = comboBox_der.Text;

            if (entra)
            {
                cargarPlantilla(nombrePlantilla_derecha, 1);
            }
        }

        /// <summary>
        /// Éste método asigna los valores necesarios a las variables e inserta la config.
        /// del cliente de ambos combo box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btInsertarConfigCliente_Click(object sender, EventArgs e)
        {
            string lista_labelsPbDerecho = "";
            string lista_labelsPbIzquierdo = "";
            string rotacionPlantilla = "";
            string positionIzq = "";
            string positionDer = "";
            string rotationIzq = "";
            string rotationDer = "";
            string font_sizeIzq = "";
            string font_sizeDer = "";
            int incremento = 1;

            foreach (var lb in label.listaLabel)
            {
                if (lb.Parent.Name.ToString() == "pbFrente")
                {
                    lista_labelsPbIzquierdo += lb.Name.ToString();
                    positionIzq += $"{lb.Location.X},{lb.Location.Y}";
                    rotationIzq += $"{lb.rotateAngle}";
                    font_sizeIzq += $"{lb.Font.Size}";
                    if (incremento != label.listaLabel.Count)
                    {
                        lista_labelsPbIzquierdo += ",";
                        positionIzq += "-";
                        rotationIzq += ",";
                        font_sizeIzq += ",";
                    }
                    incremento++;
                }
                else
                {
                    lista_labelsPbDerecho += lb.Name.ToString();
                    positionDer += $"{lb.Location.X},{lb.Location.Y}";
                    rotationDer += $"{lb.rotateAngle}";
                    font_sizeDer += $"{lb.Font.Size}";
                    if (incremento != label.listaLabel.Count)
                    {
                        lista_labelsPbDerecho += ",";
                        positionDer += "-";
                        rotationDer += ",";
                        font_sizeDer += ",";
                    }
                    incremento++;
                }
            }

            if ((pbFrente.Width == 368) && (pbFrente.Height == 187))
            {
                rotacionPlantilla = "horizontal";
            }
            else
            {
                rotacionPlantilla = "vertical";
            }

            configuracion_cliente.Nombre_cliente = CbBasedeDatos.Text;
            //configuracion_cliente.Nombre_plantilla = ComboBox_izq.Text;
            configuracion_cliente.Nombre_label = lista_labelsPbIzquierdo;
            configuracion_cliente.Posicion_label = positionIzq;
            configuracion_cliente.Tamaño_fuente = font_sizeIzq;
            configuracion_cliente.Rotacion_fuente = rotationIzq;
            configuracion_cliente.Rotacion_plantilla = rotacionPlantilla;
            configuracion_cliente.Lado = "0";
            try
            {
                if (pb.pictureBox.Parent.Name.ToString() == "pbFrente")
                {
                    configuracion_cliente.Posicion_pictureBox = $"{pb.pictureBox.Location.X.ToString()},{pb.pictureBox.Location.Y.ToString()}";
                }
                else
                {
                    configuracion_cliente.Posicion_pictureBox = "Sin registro";
                }
            }catch(Exception excepcion)
            {
                configuracion_cliente.Posicion_pictureBox = "No existente";
                MessageBox.Show("No ingresaste un picturebox");
            }


            if (CbBasedeDatos.Text != "" && ComboBox_izq.Text != "")
            {
                if (configuracion_cliente.insertar())
                {
                    MessageBox.Show("La configuracion del cliente (cbizquierdo) fue insertada");
                }
                else
                {
                    MessageBox.Show("Te la pelaste :v");
                }

                configuracion_cliente.Nombre_cliente = CbBasedeDatos.Text;
                //configuracion_cliente.Nombre_plantilla = ComboBox_der.Text;
                configuracion_cliente.Nombre_label = lista_labelsPbDerecho;
                configuracion_cliente.Posicion_label = positionDer;
                configuracion_cliente.Tamaño_fuente = font_sizeDer;
                configuracion_cliente.Rotacion_fuente = rotationDer;
                configuracion_cliente.Lado = "1";
                configuracion_cliente.Rotacion_plantilla = rotacionPlantilla;

                try
                {
                    if (pb.pictureBox.Parent.Name.ToString() == "pbVuelta")
                    {
                        configuracion_cliente.Posicion_pictureBox = $"{pb.pictureBox.Location.X.ToString()},{pb.pictureBox.Location.Y.ToString()}";
                    }
                    else
                    {
                        configuracion_cliente.Posicion_pictureBox = "Sin registro";
                    }
                }
                catch (Exception excepcion)
                {
                    configuracion_cliente.Posicion_pictureBox = "No existente";
                    MessageBox.Show("No ingresaste un picturebox");
                }

                if (configuracion_cliente.insertar())
                {
                    MessageBox.Show("La configuracion del cliente (cbderecho) fue insertada");
                }
                else
                {
                    MessageBox.Show("Te la pelaste :v");
                }
            }
            else
            {
                MessageBox.Show("Te la pelaste");
            }
        }

        #endregion

        //private void dgvBDcliente_CellClick(object sender, DataGridViewCellEventArgs e)
        //{
        //    DataGridView DGV = dgvBDcliente;
        //    id_cellData = rd.inicializarRowData(DGV, e);
        //}
    }
}

