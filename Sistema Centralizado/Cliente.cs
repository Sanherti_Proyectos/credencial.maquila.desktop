﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Sistema_Centralizado
{
    class Cliente : Query
    {
        #region atributos
        private string id_cliente = "";
        private string nombre = "";
        private string stringconnection = "";
        private string parametros = "";
        private string posicion = "";
        private string rotacion = "";
        private string tamaño_fuente = "";
        private string fecha_impresion = DateTime.Now.ToString();
        public int cantidad_impresiones = 0;
        public string contador_imp = "contador_imp";
        
        public string tabla = "";
        public string atributos_insertar = "";
        public string atributos_seleccionar = "";
        public string atributos_actualizar = "";


        #endregion

        #region constructor
        //public Cliente(string nombre, string stringconnection, string parametros)
        //{
        //    this.Nombre = nombre;
        //    this.Stringconnection = stringconnection;
        //    this.Parametros = parametros;

        //    this.tabla = CLIENTE;
        //    this.atributos_insertar = cliente_atributos_insertar;
        //    this.atributos_seleccionar = cliente_atributos_seleccionar;
        //}

        public Cliente()
        {
            
            this.tabla = CLIENTE;
            //this.atributos_insertar = cliente_atributos_insertar;
            //this.atributos_seleccionar = cliente_atributos_seleccionar;
        }
        #endregion

        public bool insertarCliente()
        {
            return insertar(queryInsertar(tabla, atributos_insertar, valores()));
        }

        public void cargarCliente(string id_cliente)
        {
            //obtenerDatos = consulta(queryConsulta(atributos_seleccionar, tabla, WHERE + id_cliente_a + igual + c + id_cliente + c), tabla);

            ///*Id_cliente = obtenerDatos.Tables[tabla].Rows[0][0].ToString();*/
            //Nombre = obtenerDatos.Tables[tabla].Rows[0][2].ToString();
            //Stringconnection = obtenerDatos.Tables[tabla].Rows[0][3].ToString();
            //Parametros = obtenerDatos.Tables[tabla].Rows[0][4].ToString();
        }

        /// <summary>
        /// llena una collecion de datos con el nombre del cliente en un comboBox
        /// </summary>
        /// <param name="cb"></param>
        /// <param name="entra"></param>
        public void llenarComboBoxCliente(ComboBox cb, ref bool entra)
        {
            llenarComboBox(cb, queryConsulta(nombre_cliente_a, tabla, ""), nombre_cliente_a, tabla, ref entra);
        }

        public string valores_contador()
        {
            return
            $"{abrir_parentesis}" +
            $"{c}" +
            $"{fecha_impresion.ToUpper()} {ccc}" +
            $"{cantidad_impresiones}" +
            $"{c}" +
            $"{cerrar_parentesis} ";
        }

        public string valoresUpdate()
        {
            return
            $"{fecha_impresion} {igual} {c}{fecha_impresion}{c} {cc}" +
            $"{cantidad_impresiones} {igual} {c}{cantidad_impresiones}{c}";
        }

        public bool insertarContador(string id)
        {
            atributos_insertar = "('contador_imp','fecha_imp')";
            string condicion_where = "WHERE id_alumno  = " + id;
            obtenerDatos = (consulta(queryConsulta(contador_imp, tabla, condicion_where),tabla));
            cantidad_impresiones = Int32.Parse(obtenerDatos.Tables[tabla].Rows[0][0].ToString());
            cantidad_impresiones++;
            atributos_actualizar = "contador_imp = "+ cantidad_impresiones  + ",fecha_imp = " + c + fecha_impresion + c;


            return insertar(queryActualizar(tabla, atributos_actualizar, condicion_where));
        }

        public void llenarDgvBDcliente(DataGridView dgv)
        {
            llenarDgv(dgv, queryConsulta("*", tabla, ""), tabla);
        }

        public string valores()
        {
            return abrir_parentesis +
                        c +
                        Nombre + ccc +
                        Stringconnection + ccc +
                        Parametros +
                        c +
                   cerrar_parentesis;
        }

        //public string ValoresActualizar()
        //{
        //    //return nombre_a + igual + c + Nombre + c +
        //    //       stringconnection_a + igual + c + Stringconnection + c +
        //    //       parametros_a + igual + c + Parametros + c;
        //}

        #region SET AND GET
        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Stringconnection
        {
            get
            {
                return stringconnection;
            }

            set
            {
                stringconnection = value;
            }
        }

        public string Parametros
        {
            get
            {
                return parametros;
            }

            set
            {
                parametros = value;
            }
        }

        public string Id_cliente
        {
            get
            {
                return id_cliente;
            }

            set
            {
                id_cliente = value;
            }
        }

        #endregion
    }
}
